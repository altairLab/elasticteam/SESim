% ======================================================================
%> @brief Converts all cells in structs in a struct.
%>
%> @param s Struct to analyze
%>
%> @retval s The input struct with all the cells converted to structs.
% ======================================================================
function s = convertCells2structs(s)
	ff = fieldnames(s);
	
	for i=1:length(ff)
		field = ff{i};
		if(iscell(s.(field)))
			s.(field) = cell2mat(s.(field));
		end
		if(isstruct(s.(field)))
			s.(field) = convertCells2structs(s.(field));
		end
	end
end