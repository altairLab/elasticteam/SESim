% ======================================================================
%> @brief Generate the reference signal used for experiments.
%>
%> @param params Parameters object
%> @param settling_time Settling time of the system
%>
%> @retval output_signal The reference signal generated
%> @retval t The time signal generated
% ======================================================================
function [output_signal,t] = reference_generator(params)
    
	% take necessary variables from parameters
	ts = params.h_physics;
	amplitude_step_1 = params.tracking.reference_signal.amplitude_step_1;
	amplitude_step_2 = params.tracking.reference_signal.amplitude_step_2;
	amplitude_step_3 = params.tracking.reference_signal.amplitude_step_3;
	amplitude_step_4 = params.tracking.reference_signal.amplitude_step_4;
	step_duration = params.tracking.reference_signal.step_duration;
	sweep_amplitude = params.tracking.reference_signal.sweep_amplitude;
	sweep_duration = params.tracking.reference_signal.sweep_duration;
	sweep_max_freq = params.tracking.reference_signal.sweep_max_freq;
    
    % first 4 steps
    output_signal = amplitude_step_1 * ones(1,ceil(step_duration/ts));
    max_t = step_duration;
    output_signal = horzcat(output_signal, amplitude_step_2 * ones(1,ceil(step_duration/ts)));
    max_t = max_t + step_duration;
    output_signal = horzcat(output_signal, amplitude_step_3 * ones(1,ceil(step_duration/ts)));
    max_t = max_t + step_duration;
	output_signal = horzcat(output_signal, amplitude_step_4 * ones(1,ceil(step_duration/ts)));
    max_t = max_t + step_duration;
    
    % final sweep
    t_sweep = 0:ts:sweep_duration;
	m = sweep_max_freq / sweep_duration; % m = final_value/final_time
	rect = (m/2)*t_sweep;
    sweep = amplitude_step_4 + sweep_amplitude * sin(2*pi*t_sweep.*rect);
    output_signal = horzcat(output_signal,sweep);
    max_t = max_t + sweep_duration;
    
    % time
    t = 0:ts:max_t;
    if(length(t) > length(output_signal))
        t = t(1:length(output_signal));
    elseif(length(output_signal) > length(t))
        output_signal = output_signal(1:length(t));
    end
    
end

