% ======================================================================
%> @brief Split the experiment into the 4 different signals.
%>
%> Split the signals in test corresponding to the different reference
%> signals.
%>
%> @param test The experiment to analyze
%> @param params Parameters object
%>
%> @retval step1 Signals when there is the first step
%> @retval step2 Signals when there is the second step
%> @retval step3 Signals when there is the third step
%> @retval final_sweep Signals when there is the sweep
% ======================================================================
function [step1,step2,step3,final_sweep] = split_signals(test,params)

	% take necessary variables from parameters
    ts = params.h_physics;
	step_duration = params.tracking.reference_signal.step_duration;
    
    % first step
    t1 = 1;
    t2 = fix(step_duration/ts);
    step1 = test(t1:t2,:);
    
    % second step
    t1 = t2 + 1;
    t2 = fix((2*step_duration)/ts);
    step2 = test(t1:t2,:);
    
    % third step
    t1 = t2 + 1;
    t2 = fix((3*step_duration)/ts);
    step3 = test(t1:t2,:);
	
	% fourth step
    t1 = t2 + 1;
    t2 = fix((4*step_duration)/ts);
    step4 = test(t1:t2,:);
    
    % final sweep
    t1 = t2 + 1;
    t2 = height(test);
    final_sweep = test(t1:t2,:);
end

