% ======================================================================
%> @brief Estimate the inertia of an experiment from a .csv file.
%>
%> @param path2file The experiment to analyze (.csv)
%>
%> @retval inertia The estimated inertia
%> @retval var The variance
% ======================================================================
function [inertia,var] = inertiaEstimator(path2file)

    % prepare data to analyze
    T = readtable(path2file);
    ts = mean(diff(T.t));
    cut_off_freq = 10; % [Hz]
    half_sampling_freq = 1/(2*ts);
    [B,A] = butter(8,cut_off_freq/half_sampling_freq);

	% filter data
    dtheta_m = filter(B,A, T.dtheta_m);
    ddtheta_m = filter(B,A, T.ddtheta_m);
    tau_s = filter(B,A, T.tau_s);

	% set y and phi for the LSIdentification
    y = -tau_s;
    phi = [ddtheta_m];    

    % identification
    [lambda,~,~,~,var_lambda] = LSIdentification(phi,y,2, 1);
    
    inertia = lambda(1);
    var = var_lambda(1);
end
