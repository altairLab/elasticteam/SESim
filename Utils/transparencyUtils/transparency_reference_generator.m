% ======================================================================
%> @brief Generate the transparency reference and time signals.
%>
%> @param params Parameters object
%>
%> @retval output_signal The transparency reference signal generated
%> @retval t The time signal generated
% ======================================================================
function [output_signal,t] = transparency_reference_generator(params)

	% get necessary variables from parameters
	ts = params.h_physics;
    sweep_amplitude = params.transparency.reference_signal.sweep_amplitude;
	sweep_duration = params.transparency.reference_signal.sweep_duration;
	sweep_max_freq = params.transparency.reference_signal.sweep_max_freq;
	
	% time
    t = 0:ts:sweep_duration;
	
	% sweep
	m = sweep_max_freq / sweep_duration; % m = final_value/final_time
	rect = (m/2)*t;
    output_signal = sweep_amplitude * sin(2*pi*t.*rect);
    
	% check signals size
	if(length(t) > length(output_signal))
        t = t(1:length(output_signal));
    elseif(length(output_signal) > length(t))
        output_signal = output_signal(1:length(t));
	end
end