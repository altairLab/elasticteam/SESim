%> Environment disturbance as sweep for transparency PI


function sweep = disturbance_sweep()

    global simulation

    params = simulation.params;

    
%   time_vector = 0:h_physics:sweep_duration-1/h_physics;

%   initializing sweep vector
    sweep = zeros(1/params.h_physics * params.transparency_duration,1);


%   establish the frequency step to reach the theta_max
    


    t_sweep = 0:params.h_physics:params.transparency_duration;

    
	m = params.theta_bw / params.transparency_duration; % m = final_value/final_time
	rect = (m/2)*t_sweep;
    sweep =  params.theta_max * sin(2*pi*t_sweep.*rect);




end

% plot(time_vector,outputsignal,'Color','Blue')
% axis([0 56 0 1.6])
% title('Four steps and sweep from 1 to 15 Hz')
% xlabel('Time')
% ylabel('Amplitude')