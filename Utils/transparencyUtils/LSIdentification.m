function [lambda sigma phi y var_lambda] = LSIdentification(phi,y, confidence, max_iteration_number)
% Least Square identification
% A least square procedure that iterates several times and eliminates
% outliers
% confidence: for removing outliers
% max_iteration_number: the maximum number of iteration
%
% EXAMPLES:
% [lambda sigma] = LSIdentification(phi,y, 3, 14)
% does 14 cycles and eliminates ouliers out of 3 sigma confidence

retain_indexes =1:length(phi);

% identification cycle
for i=1:max_iteration_number 
    
    y = y(retain_indexes');
    phi = phi(retain_indexes,:);
    
    % least squares
    
    lambda = (phi' * phi) \ phi' * y;
    var_lambda = inv(phi' * phi);
    
    % model which approsimate experimental data
    y_model = phi * lambda;
    
    % error between model and data
    error = y - y_model;

    % std of error
    sigma = sqrt(var(error));
    
    
    % eliminating data which is outer than the model approximation
    % we suppose is gaussian noise
    retain_indexes = find(abs(error) < sigma*confidence);
    
    if(length(retain_indexes) == length(y)) 
        break;
    end
    
    
end

% 
% figure(999)
% % Experiment
% plot(y)
% hold on
% pause
% % Model (green)
% plot(y_model,'g')
% % Error (red)
% plot(error,'r')
% hold off
% legend('y','model','error')

