% ======================================================================
%> @brief Clean the simulink cache files.
% ======================================================================
function clean_sim()
	d = dir('slprj');
	if isempty(d)==false
		rmdir('slprj','s');
	end

	filelist = dir(['*.slxc']);
	for i=1:length(filelist)
		delete([filelist.folder '/' filelist(i).name]);
	end
end