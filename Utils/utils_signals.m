% ======================================================================
%> @file utils_signals.m
%> @brief A collection of some standard types of signals.
% ======================================================================


% duration of the signals below
utils.duration = 10;	% [s]

% time signal for the signal below
utils.t = 0:params.h_physics:utils.duration;

% standard signals
utils.impulse = utils.t == 0;				% impulse at t==0
utils.unitstep = utils.t >= 0;				% step starting from t==0
utils.ramp = utils.t.*utils.unitstep;       % ramp starting from t==0
utils.quad = utils.t.^2.*utils.unitstep;    % quad starting from t==0
utils.unitsin = sin(2*pi*1*utils.t);		% sin freq==1
utils.unitsin_offset = 1+sin(2*pi*1*utils.t);
utils.unitquad = sign(sin(2*pi*1*utils.t));	% quadrature signal
utils.unitquad_offset = 1+sign(sin(2*pi*1*utils.t));
% utils.sweep = sweep_gen(params,utils.duration);
% utils.disturbance_sweep = disturbance_sweep();