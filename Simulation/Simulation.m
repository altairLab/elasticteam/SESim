% ======================================================================
%> @brief Class used to do simulations.
%>
%> It contains params, model, controller and also the reference signal.
% ======================================================================
classdef Simulation < handle
    properties (GetAccess = public, SetAccess = private)
		%> Parameters object
        params
		%> IModel object
        model
		%> IController object
        controller
		
		%> The current step in the actual simulation
        index
		%> The reference signal
		ref
    end
    properties (Access = private)
		%> Name of the simulation
		sim_model_name
	end
    
    methods
		% ======================================================================
		%> @brief Class constructor.
		%>
		%> @param sim_model_name Name of the simulation
		%>
		%> @return instance of the Simulation class.
		% ======================================================================
        function obj = Simulation(sim_model_name)
			obj.sim_model_name = sim_model_name;
		end
        
		% ======================================================================
		%> @brief Set the params property.
		%>
		%> @param params Parameters object
		%>
		%> @return Returns false if params is not a Parameters object, true
		%> otherwise.
		% ======================================================================
        function res = setParams(obj,params)
            if isa(params, 'Parameters')
                obj.params = params;
                res = true;
            else
                res = false;
            end
		end
        
		% ======================================================================
		%> @brief Set the model property.
		%>
		%> @param model Name of the IModel to use
		%>
		%> @return Returns false if model is not a IModel object, true
		%> otherwise.
		% ======================================================================
        function res = setModel(obj,model)
			if isa(eval([char(model) '()']), 'IModel')
				obj.model = eval([char(model) '()']);
				res = true;
			else
				res = false;
			end
		end
        
		% ======================================================================
		%> @brief Set the controller property.
		%>
		%> @param controller Name of the IController to use
		%>
		%> @return Returns false if controller is not a IController object,
		%> true otherwise.
		% ======================================================================
        function res = setController(obj,controller)
			if isa(eval([char(controller) '()']), 'IController')
				obj.controller = eval([char(controller) '()']);
				res = true;
			else
				res = false;
			end
		end
        
		% ======================================================================
		%> @brief Increment the index property by 1.
		% ======================================================================
        function nextStep(obj)
            % increment the index
            obj.index = obj.index + 1;
		end
		
		% ======================================================================
		%> @brief Run the simulation.
		%>
		%> @param ref Reference signal
		%> @param duration Total duration of the simulation in seconds
		% ======================================================================
		function run(obj,ref,duration)
            % run simulink for the settling time simulation
			obj.ref = [];
            obj.ref = ref;
			
			% reset all variables and objects
			obj.reset();

			% close previous models with the same name
			close_system(obj.sim_model_name,0);
			
			% set h_physics
			assignin('base','h_physics',obj.params.h_physics)
			
			% simulate the model
            sim(obj.sim_model_name, duration);
		end
        
		% ======================================================================
		%> @brief Reset all the properties.
		% ======================================================================
        function reset(obj)
            % throw errors if it's not all set correctly
            if ~isa(obj.params, 'Parameters')
                error(['Parameters not set in simulation. Set the ' ...
                    'parameters before calling the reset method.']);
            end
            if ~isa(obj.model, 'IModel')
                error(['Model not set in simulation. Set the ' ...
                    'model before calling the reset method.']);
            end
            if ~isa(obj.controller, 'IController')
                error(['Controller not set in simulation. Set the ' ...
                    'controller before calling the reset method.']);
            end
            
            % call the reset methods
            obj.model.reset();
            obj.controller.reset();
            
            obj.index = 0;
        end
    end
end