% This script does two simulations for the transparency PI: 
% one with the controller on and the other with the controller off.

% remove useless temporary files
clean_sim();

% Load system parameters with user configuration
configuration;

% load transparency model and controller
simulation.setModel(model_transparency);
simulation.setController(controller_transparency);

% matrix for the csv
matrix4csv = [];

try % the first cycle is with the controller the second is without controller
    for i=1:2
        
        % run the simulation with the step ref
		[ref,t] = transparency_reference_generator(params);
        simulation.run(ref,max(t));

        % save the simulation output
        simulation_output.t = simulation.controller.time;
        simulation_output.ref = simulation.controller.ref;
        simulation_output.tau = simulation.controller.tau;
        simulation_output.err = simulation.controller.ref - ...
            simulation.controller.tau;

        % convert the results in a matrix
        clear matrix4csv
        matrix4csv(:,1) = simulation_output.t';
        matrix4csv(:,2) = simulation_output.ref';
        matrix4csv(:,3) = simulation_output.tau';
        matrix4csv(:,4) = simulation_output.err';
        matrix4csv(:,5) = simulation.controller.theta_m;
        matrix4csv(:,6) = simulation.controller.dtheta_m;
        matrix4csv(:,7) = simulation.controller.ddtheta_m;
        matrix4csv(:,8) = simulation.controller.tau_s;
        matrix4csv(:,9) = simulation.controller.tau_m;
        matrix4csv(:,10) = simulation.controller.theta_e;

        if i == 1
            type = 'with_control';
            simulation.controller.enable_output = false;
        else
            type = 'without_control';
            simulation.controller.enable_output = true;
        end

        % convert the results in a table, then save it in a .csv file
        table4csv = array2table(matrix4csv,'VariableNames',{'t','ref','tau','err','theta_m','dtheta_m','ddtheta_m','tau_s','tau_m','theta_e'});
        csv_name = [simulation_dir simulation_name ',transparency=' type '.csv'];
        writetable(table4csv,csv_name);

        % DEBUG plot
        figure('NumberTitle', 'off', 'Name', simulation.model.type + ' ' + type);
        plot(simulation_output.t',[simulation_output.ref' simulation_output.tau']);
    end
catch e
    if isa(e,'MSLException')
        "ERROR: Simulation unstable. Change parameters and retry." + e.message
		e.getReport()
    else
        "ERROR: " + e.message
        e.getReport()
    end
end


