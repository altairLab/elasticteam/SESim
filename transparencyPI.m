% ======================================================================
%> @brief Compute the transparency index.
%>
%> The transparency index is a value >= 0. It represents how much the
%> system can decrease the perceived inertia of the environment. A value of
%> 0 means that the controller does not decrease the inertia perceived (so
%> the system is no transparent).
%>
%> @param params Parameters object
%> @param experiment_table The table of the experiment to analyze
%>
%> @retval index The linearity index
%> @retval labels The corresponding frequency labels
% ======================================================================
function [index,labels] = transparencyPI(params,table_no_control,table_with_control)
	experiments{1} = table_no_control;
	experiments{2} = table_with_control;
	inertias = {};
	for j = 1:2
		% take the table and start the time from 0
		T = experiments{j};
		T.time(:) = T.t(:) - T.t(1); 

		% how many Hz consider for each step analisys
		interval = 1; % [Hz]

		% reference signal variables
		sweep_max_freq = params.transparency.reference_signal.sweep_max_freq;
		duration = max(T.t);

		% the matrix to compose all the sub-csvs
		matrix4analisys = {};

		% data to calculate the inertia with LeastSquare method
		timeVect = T.t;
		dtheta_m = T.dtheta_m;
		ddtheta_m = T.ddtheta_m;
		tau_s = T.tau_s;
		ts = 1/mean(diff(timeVect));

		% number of samples per sub-csv
		samples = floor(duration/sweep_max_freq * ts * interval);

		% an error while calculating the second derivative of motor movement
		if ddtheta_m(1) == Inf || isnan(ddtheta_m(1)) || ddtheta_m(1) == -Inf
			ddtheta_m(1) = 0;
		end

		% csv split
		for i = 1:(length(timeVect)/samples)
			if i > 1
				matrix4analisys{1,i} = timeVect((i-1)*samples:i*samples);
				matrix4analisys{2,i} = dtheta_m((i-1)*samples:i*samples);
				matrix4analisys{3,i} = ddtheta_m((i-1)*samples:i*samples);
				matrix4analisys{4,i} = tau_s((i-1)*samples:i*samples);
			else
				matrix4analisys{1,i} = timeVect(1:samples);
				matrix4analisys{2,i} = dtheta_m(1:samples);
				matrix4analisys{3,i} = ddtheta_m(1:samples);
				matrix4analisys{4,i} = tau_s(1:samples);
			end
		end

		% temporary directory where to save files 
		temporary_folder = [params.simulation_output_dir '.TempTransparency'];
		if(exist(temporary_folder, 'dir'))
			rmdir(temporary_folder,'s');
		end
		mkdir(temporary_folder);
		
		% save in temporary .csv files the tables
		for i = 1:(length(timeVect)/samples)
			table4csv(:,1) = (matrix4analisys{1,i});
			table4csv(:,2) = (matrix4analisys{2,i});
			table4csv(:,3) = (matrix4analisys{3,i});
			table4csv(:,4) = (matrix4analisys{4,i});
			table4csv = array2table(table4csv,'VariableNames',{'t','dtheta_m','ddtheta_m','tau_s'});
			csv_name = [temporary_folder '/transparency_test_' num2str(i) '.csv'];
			writetable(table4csv,csv_name);
			clear table4csv
		end

		% calculate inertia of each .csv
		inertiaArray = [];
		for i = 1:(length(timeVect)/samples)
			fName = [temporary_folder '/transparency_test_' num2str(i) '.csv'];
			[inertiaArray(i),variance{1,i}] = (inertiaEstimator(fName));
		end

		% calculate the 95 percentile
		sigma = {};
		for i = 1:length(variance)
			sigma{1,i} = inertiaArray(i)+3*sqrt(variance{1,i});
			sigma{2,i} = inertiaArray(i)-3*sqrt(variance{1,i});
		end

		labels = {}; % x axis labels
		labelsPos = []; % position of labels
		lines = []; % position of vertical lines

		% plot the experiment result
		for i = 1:(length(timeVect)/samples)
% 			plot(matrix4analisys{1,i},matrix4analisys{4,i}) 
% 			hold on
			lines(i) = matrix4analisys{1,i}(end);
% 			xline(lines(i));
			labelsPos(i) = matrix4analisys{1,i}(end);
			labels{1,i} = strcat(num2str(i*interval),'Hz');
		end

% 		xticks(labelsPos)
% 		xticklabels(labels)
% 		hold off
% 
% 		figure;

		inertia4plot = zeros(length(timeVect),1);
		first_sigma_plot = zeros(length(timeVect),1);
		last_sigma_plot = zeros(length(timeVect),1);

		for i = 1:(length(timeVect)/samples)
			if i > 1
				inertia4plot((i-1)*samples:i*samples) = inertiaArray(i);
				first_sigma_plot((i-1)*samples:i*samples) = sigma{1,i};
				last_sigma_plot((i-1)*samples:i*samples) = sigma{2,i};
			else
				inertia4plot(1:samples) = inertiaArray(i);
				first_sigma_plot(1:samples) = sigma{1,i};
				last_sigma_plot(1:samples) = sigma{2,i};
			end
		end

		% plot the inertia result
% 		time = table2array(T(:,1));

% 		p1 = plot(time,inertia4plot);
% 		hold on
% 		p2 = plot(time,first_sigma_plot,'Color','red');
% 		p3= plot(time,last_sigma_plot,'Color','green');
% 
% 		xlim([-1 max(time)*1.05])
% 		xticks(labelsPos)
% 		xticklabels(labels)
% 
% 		for i = 1:(length(lines))
% 			xline(lines(i));
% 		end
% 
% 		figure
% 		bPlot = bar(inertiaArray,'FaceColor','flat');
% 
% 		for i = 1:length(inertiaArray)
% 		   if ( true ||  abs( inertiaArray(i) / sigma{1,i} ) <  0.75 || inertiaArray(i) < 0 )
% 			   bPlot.CData(i,:) = [1 0 0];
% 		   end
% 		end

		inertias{j} = inertiaArray;
		
		% remove the temporary folder
		rmdir(temporary_folder,'s');
	end
	
	inertia_no_control = inertias{1};
	inertia_control = inertias{2};
	
	% offset the result to make it start from 0
	index = inertia_no_control./inertia_control - 1;
	% set zero for the negative results
	index(index < 0) = 0;
end

