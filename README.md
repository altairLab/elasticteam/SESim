# FORECAST

<!-- [![license - apache 2.0](https://img.shields.io/:license-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

Copyright CSIC & Tecnalia 2020

This is an example of Performance Indicator implemented in Octave.
It is prepared to be used within the Eurobench Benchmarking Software. -->

## Purposes

Benchmarking a force control algorithm within different types of environment.

<br>

## Installation

The code needs Matlab and Simulink to run.

<br>

## Documentation

The documentation requires Doxygen. Write in the terminal:
```console
cd DoxygenMatlab/
doxygen
```
Then the documentation can be viewed opening in the browser `Doc/html/index.html`. If you have Firefox installed just write in the terminal:
```console
firefox Doc/html/index.html
```

<br>

## Usage

There are two different scripts:
- `mainForecastSimulations` to do simulations
- `mainForecastPI` to do the benchmarking

---

The script `mainForecastSimulations` requires Matlab and Simulink. It executes the simulation of the system in the given range of environments.

First step is to create `simulation_name.yaml`, file that contains all the parameters, in the `simulation_dir` directory. An example can be found in this repository.

Then, to run the simulations, write on the terminal:

```console
matlab -batch "mainForecastSimulations 'simulation_dir' 'simulation_name' 'configuration_name';exit"
```

In the 'simulation_dir' directory there will be the result of the simulations.

---

The script `mainForecastPI` requires Matlab. It computes the performance indicators on a given dataset.

To run the performance indicators computation, write:

```console
matlab -batch "mainForecastPI 'simulation_dir' 'simulation_name' 'configuration_name'; exit"
```

where 'simulation_dir' is the directory, 'simulation_name' is the name of the .yaml file containing the parameters and 'configuration_name' is the name of the configuration preset in the .yaml file.

Assuming folder `simulation_outputs/` contains the simulations data, `simulation1.yaml` is the .yaml file with `conf1` as preset, the shell command is:

```console
matlab -batch "mainForecastPI 'simulation_outputs' 'simulation1' 'conf1'; exit"
```

<br>

## Example

An example is provided in the `simulation_output_example/` directory. It contains the simulation input and output data, as well as the input and output data of the PIs computation.

<br>

## Results

The output of the simulations will be found in `'simulation_dir'` directory, while the results of the performance indicator computations will be found in `'simulation_dir'/pi_output/`.

Results of PIs are plots (in .png files) and datas (in .yaml files).

<br>

## Acknowledgements

<a href="http://eurobench2020.eu">
  <img src="http://eurobench2020.eu/wp-content/uploads/2018/06/cropped-logoweb.png"
       alt="rosin_logo" height="60" >
</a>

Supported by Eurobench - the European robotic platform for bipedal locomotion benchmarking.
More information: [Eurobench website][eurobench_website]

<img src="http://eurobench2020.eu/wp-content/uploads/2018/02/euflag.png"
     alt="eu_flag" width="100" align="left" >

This project has received funding from the European Union’s Horizon 2020
research and innovation programme under grant agreement no. 779963.

The opinions and arguments expressed reflect only the author‘s view and
reflect in no way the European Commission‘s opinions.
The European Commission is not responsible for any use that may be made
of the information it contains.

[eurobench_logo]: http://eurobench2020.eu/wp-content/uploads/2018/06/cropped-logoweb.png
[eurobench_website]: http://eurobench2020.eu "Go to website"
