try
    for i = 1:params.tracking.environment.KES_NUM_ELEM
        for j= 1:params.tracking.environment.JES_NUM_ELEM

            % update simulation params
            params = simulation.params;
            params.tracking.environment.index_ke = i;
            params.tracking.environment.index_je = j;

            % set the new parameters
            simulation.setParams(params);

            % create the reference signal
            clear simulation_output
            ref = [];
            [ref,simulation_output.t] = reference_generator(params);

            if(max_duration < max(simulation_output.t))
                max_duration = max(simulation_output.t);
            end

            % run the simulation
            simulation.run(ref,max(simulation_output.t));

            % save the simulation output
            simulation_output.ref = simulation.controller.ref;
            simulation_output.tau = simulation.controller.tau;
            simulation_output.err = simulation.controller.ref - simulation.controller.tau;

            % transform the simulation output in matrix and table
            clear simulation_output_matrix
            simulation_output_matrix(:,1) = simulation_output.t';
            simulation_output_matrix(:,2) = simulation_output.ref';
            simulation_output_matrix(:,3) = simulation_output.tau';
            simulation_output_matrix(:,4) = simulation_output.err';
            simulation_output_table = array2table(simulation_output_matrix,'VariableNames',{'t','ref','tau','err'});

            % find and save the settling time of the system
            [step1,~,~,~] = split_signals(simulation_output_table,params);
            t_settling = settling_time_index(step1,params,10);

            % step analysis (includes bandwidth)
            step_analysis_index(step1,params,10)

            % save the simulation output table in a .csv file
            csv_name = [simulation_dir simulation_name ',ke=' num2str(params.ke) ',je=' num2str(params.je) ',tsettling=' num2str(t_settling) 'end.csv'];
            writetable(simulation_output_table,csv_name);
        end
    end

    % save the parameters in a .yaml file
    params.writeToYamlFile(simulation_name,configuration_name);

catch e
    if isa(e,'MSLException')
        "ERROR: Simulation unstable. Change parameters and retry." + e.message
        e.getReport()
    else
        "ERROR: " + e.message
        e.getReport()
    end
end

% run the transparency simulation if necessary
if(simulation.params.transparency.enable_transparency == true)
    mainTransparency;
end

% merge all .csv files in one
Unique_csv(simulation_dir, simulation_name, configuration_name);