% Return the sizes, initial conditions, and sample times for the S-function.
function [sys, x0, str, ts] = mdlInitializeSizes
    global simulation
    model = simulation.model;
    controller = simulation.controller;
    controller.reset();
    
    sizes = simsizes;

    sizes.NumContStates  = model.num_states;
    sizes.NumDiscStates  = 0;
    sizes.NumOutputs     = model.num_outputs;
    sizes.NumInputs      = model.num_inputs;
    sizes.DirFeedthrough = 0;
    sizes.NumSampleTimes = 1;   % At least one sample time is needed.

    sys = simsizes(sizes);

    % Initialise the initial conditions.
    x0 = model.get_initial_state();
    model.reset();
    str = [];
    ts = [0, 0];

% Return the derivatives for the continuous states.
function sys = mdlDerivatives(t, x, u)
    global simulation
    sys = simulation.model.fun_derivatives(t,x,u);

% Handle discrete state updates, sample time hits, and major time step
% requirements.
function sys = mdlUpdate(t, x, u)
    global simulation
    sys = simulation.model.fun_update(t, x, u);

% Return the block outputs.
function sys = mdlOutputs(t, x, u)
    global simulation
    sys = simulation.model.fun_outputs(t, x, u);
