methods
    function x = get_initial_state(obj)
        x = zeros(1, obj.num_states);
    end
    
    function sys = fun_derivatives(t, x, u)
        assert(false, 'IMPLEMENT THE DERIVATIVES FUNCTION!');
        sys = zeros(obj.num_states, 1);
    end
    
    function sys = fun_update(t, x, u)
        assert(false, 'IMPLEMENT THE UPDATE FUNCTION!');
        sys = [];
    end
    
    function sys = fun_outputs(t, x, u)
        assert(false, 'IMPLEMENT THE OUTPUT FUNCTION!');
        sys = [];
    end
end