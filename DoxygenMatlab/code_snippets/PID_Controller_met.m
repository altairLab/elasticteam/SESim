function [U,tau] = process_(obj, t, theta_m, dtheta_m, theta_e, dtheta_e, ref)
    % necessary to retrieve the index
    global simulation;
    index = simulation.index;
    params = simulation.params;

    dref = 0;
    if(t-obj.prev_t ~= 0)
        dref = (ref - obj.prev_ref) / (t - obj.prev_t);
    end

    % compute the three different errors
    err = ref - tau_s;
    derr = dref - dtau_s;
    obj.integrative_error = obj.integrative_error + err;

    % PID law
    U = obj.kp * err + obj.kd * derr + obj.ki * obj.integrative_error;

    % current saturation
    if(params.motor.current_saturation>0 && abs(U)>params.motor.current_saturation)
        U = sign(U) * params.motor.current_saturation;
    end
end