properties (GetAccess = public, SetAccess = private)
    time    % time signal
    ref     % reference signal
    tau     % force signal

    prev_U      % processed value in the previous step
    prev_ref    % reference value in the previous step
    prev_t      % time value in the previous step
end
properties (GetAccess = public, SetAccess = protected)
    name    % name of the controller

    % General purpose debug signals
    debug1
    debug2
    debug3
end
properties (Access = public)
    % Variables used to manage the control loop frequency
    control_loop_max
    control_loop_count
end