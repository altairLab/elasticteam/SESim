function U = process(obj, t, theta_m, dtheta_m, theta_e, dtheta_e, ref)
    % necessary to retrieve the index
    global simulation
    index = simulation.index;

    U = obj.prev_U;

    % enter in this loop only with a frequency of 1/h_control
    obj.control_loop_count = obj.control_loop_count + 1;
    if(obj.control_loop_count >= obj.control_loop_max)
        obj.control_loop_count = 0;

        % call the process of the child
        U = obj.process_(t,tau_s,dtau_s,theta_m,dtheta_m,theta_e,dtheta_e,ref);

        % update previous controller values
        obj.prev_ref = ref;
        obj.prev_t = t;
        obj.prev_U = U;
    else
        % update arrays with previous values
        obj.debug1(index) = obj.debug1(index-1);
        obj.debug2(index) = obj.debug2(index-1);
        obj.debug3(index) = obj.debug3(index-1);
    end

    % update arrays with new values
    obj.time(index) = t;
    obj.ref(index) = ref;
    obj.tau(index) = tau_s;
end

function [U,tau] = process_(obj, t, theta_m, dtheta_m, theta_e, dtheta_e, ref)
    assert(false, 'IMPLEMENT THE CONTROLLER!');
    U = 0;
    tau = 0;
end