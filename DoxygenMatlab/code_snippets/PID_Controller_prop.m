properties (Access = public)
    kp          % proportional gain
    kd          % derivative gain
    ki          % integrative gain
    integrative_error        % integrative of error
end