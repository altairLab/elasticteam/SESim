properties (GetAccess = public, SetAccess = public)
    %> simulation parameter - physics update period
    h_physics
    %> simulation parameter - control update period
    h_control

    %> structure for the motor parameters
    motor
    %> structure for the motor controller parameters
    motor_controller

    %> structure for the transparency parameters
    transparency
    %> structure for the tracking parameters
    tracking
end
properties (GetAccess = public, SetAccess = private)
    %> directory of the simulation output
    simulation_output_dir

    %> configuration name
    config_name
end