function x = get_initial_state(obj)
    x = [0, 0, 0, 0];
end

function sys = fun_derivatives(obj, t, x, u)
    global simulation
    params = simulation.params;
    
    sys(1) = x(2);
    sys(2) = (1 / params.Jm) * (obj.U - params.k_spring * (x(1) - x(3)) - params.dm * x(2));
    sys(3) = x(4);
    sys(4) = (1 / params.Je) * (- params.de * x(4) - params.ke * x(3) + params.k_spring * (x(1) - x(3)));
end

function sys = fun_update(obj, t, x, u)
    global simulation
    params = simulation.params;
    controller = simulation.controller;
    sys = [];
    
    ref = simulation.ref(obj.ref_index);
    obj.ref_index = obj.ref_index + 1;
    obj.U = controller.process(t, x(1), x(2), x(3), x(4), ref);
    obj.prev_update_t = t;
end

function sys = fun_outputs(obj, t, x, u)
    sys = [x(1) x(2) x(3) x(4)];
end

% [...] other code