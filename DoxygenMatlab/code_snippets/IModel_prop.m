properties
    type
    num_states      % number of states
    num_outputs     % number of outputs
    num_inputs      % number of inputs
    ref_index       % index used in the fun_update()
end