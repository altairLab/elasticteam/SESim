%> @file  documentation_index.m
%> @brief SESim introduction
%======================================================================
%> @mainpage SESim introduction
%>
%> @section intro Introduction
%>
%> The @b SESim software allows to simulate and evaluate force control algorithms.
%> The software implementation is divided in two distinct parts:
%> - Simulation
%> - Performance Indicators (PIs) evaluation
%>
%> Each part is designed to work standalone. The main reason for this division is that the evaluation of the PIs must be available for both real and virtual experiments.
%>
%> @section simulation Simulation
%>
%> The simulation software is written in Matlab and uses also Simulink to execute the system. This software allows the user to execute multiple simulations of the system of interest considering the necessary reference signals needed to calculate the PIs. The user can chose an actuator model among already implemented models and can implement his own controller, or chose among already implemented ones.
%> Finally the user need to set some simulation parameters. The software performs the simulations for all the given environments, chosen by the user, ans saves the results in specified <em>.csv</em> files. These files contain all the data for the PIs evaluation.
%>
%> The simulation parameters required by the user are stored in a .yaml
%> file. Here an example with an explanation of the main parameters:
%> \include example_input.yaml
%> 
%> Implemented models:
%> - SEA current motor
%> - (not yet) SEA voltage motor
%>
%> Available implemented controllers:
%> - PID controller
%> - PID controller with AF
%>
%> We will show the <b>SEA current motor model</b> and the <b>PID controller</b> more in depth later.
%>
%>
%> @subsection files_composition Files composition
%> Although there exist two distinct softwares for simulation and evaluation, they share some files and directories.
%>
%> Files hierarchy is as follow:
%> - <b>Simulation.m</b>: class used to do a simulation
%> - <b>Simulation/sim\_model.mdl</b>: simulink model used in the simulation
%> - <b>Parameters.m</b>: class used to store some parameters used in the simulations
%> - <b>mainForecastPI.m</b>: file to run to calculate PIs on given data (explained in the next section)
%> - <b>mainForecastSimulations.m</b>: file to run multiple simulations
%> - <b>configuration.m</b>: file that loads the parameters from the .yaml file
%> - <b>Utils/</b>: directory that contains some utility functions
%> - <b>TaskLoader/task\_loader.m</b>: function that loads in the workspace some predefined signals
%> - <b>Simulation/Models/</b>: directory with all the implemented system models
%> - <b>PerformanceIndicators/</b>: files present here will be explained in the next section
%> - <b>Controller/</b>: directory that contains all the implemented system controllers
%>
%> @subsection configuration Configuration
%> In the configuration.m file is set the model and the controller of the
%> transparency experiments. This should not be modified by the user.
%>
%>
%> @subsection task_loader Task loader
%> In the task_loader.m file, the user has access to some standard signal, i.e. <em>impulse(t), step(t) and ramp(t)</em>.
%>
%>
%> @subsection simulink_model Simulink model
%> \image html sim_model.svg
%>
%> The simulink model is simple, there are 3 blocks (from left to right):
%> - the first contains the <b>inputs</b>, i.e. the force reference
%> - the second is an <b>S-Fun</b> block (explained in details below)
%> - the third saves the simulation <b>outputs</b> to the Matlab workspace
%>
%>
%> @subsection s_function S-Function
%> S-functions (system-functions) provide a powerful mechanism for extending the capabilities of the Simulink environment.
%> An S-function is a computer language description of a Simulink block written in MATLAB or other languages such as C, C++, and Fortran.
%> S-functions use a special calling syntax called the S-function API that enables you to interact with the Simulink engine.
%> By following a set of simple rules, you can implement an algorithm in an S-function and use the S-Function block to add it to a Simulink model.
%> S-functions define how a block works during different parts of simulation, such as initialization, update, derivatives, outputs and termination.
%> In every step of a simulation, a method is invoked by the simulation engine to fulfill a specific task.
%>
%>
%> @subsubsection how_it_works How it works
%> Execution of a Simulink model proceeds in stages.
%> - <b>Initialization phase</b> where the Simulink engine incorporates library blocks into the model, propagates signal widths, data types, and sample times, evaluates block parameters, determines block execution order, and allocates memory.
%> - <b>Simulation phase</b>, where the engine enters in a simulation loop, where each pass through the loop is referred to as a simulation step. During each simulation step, the engine executes each block in the model and, for each block, the engine invokes functions that compute the block states, derivatives, and outputs for the current sample time. The entire simulation loop then continues until the simulation is complete.
%>
%> An S-function comprises a set of S-function callback methods that perform tasks required at each simulation stage. During simulation of a model, at each simulation stage, the Simulink engine calls the appropriate methods for each S-Function block in the model. Tasks performed by S-function callback methods include:
%> - Calculation of outputs: outputs are calculated.
%> - Update discrete states: the block performs once-per-time-step activities such as updating discrete states.
%> - Initialize and Terminate Methods: perform initialization and termination activities required by S-function only once, e.g. initializing state vectors or freeing memory.
%> - Integration: the solvers can compute the integration of the variables.
%>
%>
%> @subsubsection our_implementation_of_sfun Our implementation of sfun.m
%> \include sfun.m
%>
%> As one can see from the code, sfun.m requires the wrapper of our object-oriented code which holds model,
%> controller and parameters for the ongoing simulation. To exploit the advantages of S-Fun,
%> we will see that models and controllers override the implementation of <em>fun_derivatives(), fun\_update() and fun\_outputs()</em>.
%>
%>
%> @section software_architecture Software architecture and class hierarchy
%> \image html overview_classes_scheme.svg
%>
%>
%> @subsection parameters Parameters
%> Parameters is a class with dynamic parameters (it inherits from <em>dynamicprops</em>) used to store the parameters for the simulation and evaluation.
%> Standard parameters are the following:
%>
%> \include Parameters.m
%>
%> Thanks to the dynamicprops, there can be other parameters added by the user. <br>
%> Parameters has a method to save all the parameters in a <em>.yaml</em> file, and also a method to read all the parameters from a <em>.yaml</em> file.
%>
%> @subsection simulation Simulation
%> \image html simulation_class_diagram.svg
%>
%> Simulation is a class that inherits from <em>handle</em>, which is an abstract class used to implement subclasses that inherit handle behavior.
%> In this way more than one variable can refer to the same handle object because they actually holds a reference to the object.
%> Simulation is a wrapper of our object-oriented code that contains the parameters, the model and the controller of the actual experiment.
%>
%> \subsection{Models}
%> \image html model_class_diagram.svg
%>
%> IModel is a class that inherits from <em>handle</em>. All model implementations must have IModel as superclass.
%>
%> @subsubsection imodel IModel
%> The properties are:
%> \include IModel_prop.m
%> While the methods are:
%> \include IModel_met.m
%> Asserts are present to help the user to override all the required IModel methods.
%>
%>
%> @subsubsection implemented_models Implemented models
%> Among implemented models we provide a <b>SEA with current controlled motor</b>.
%> This model represents the equation of a SEA, seen in equation \ref{eqn:sea},
%> with a current motor as seen in the equation \ref{eqn:motor_model}, with spring stiffness \f$K_s\f$,
%> motor damping \f$d_m\f$ and motor inertia \f$J_m\f$, and in contact with a second order environment
%> with inertia \f$J_e\f$, damping \f$d_e\f$ and stiffness \f$K_e\f$. Clearly its superclass is IModel.
%> It's important to know that it has 4 states, 4 outputs and 0 inputs.
%> \include Current_met.m
%> All the <em>params</em> are inside the Parameters.m file. From the code we see that the model calls the controller.
%>
%>
%> @subsection controllers Controllers
%> \image html controller_class_diagram.svg
%>
%> IController is a class that inherits from <em>handle</em>. All controllers implementations must have IController as superclass.
%>
%>
%> @subsubsection icontroller IController
%> The properties are:
%> \include IController_prop.m
%> While the main methods are:
%> \include IController_met.m
%> Assert is present to help the user to override all the IController methods.
%>
%>
%> @subsubsection implemented_controllers Implemented controllers
%> There are already available:
%> - <b>PID controller</b>, with settable proportional \f$K_p\f$, integrative \f$K_i\f$ and derivative \f$K_d\f$ gain constants;
%> - <b>PID controller with AF</b>, with settable proportional \f$K_p\f$, integrative \f$K_i\f$, derivative \f$K_d\f$ and acceleration feedback \f$af\f$ gain constants.
%>
%> For the sake of conciseness, we will show only PID controller without AF.
%> \include PID_Controller_prop.m
%> These properties are settable directly by the user, after the controller is constructed.
%> \include PID_Controller_met.m
%> It's easy to notice the control law of this PID, that is:
%> \f[
%>    U = K_p \: e + K_d \: \dot{e} + K_i \: \int e \: dt
%> \f]
%> where \f$e = \tau_{ref} - \tau\f$ is the error.
%>
%>
%> @subsection simulation_steps Simulation steps
%> The main function is mainForecastSimulations.m and it's used to run all the simulations. It takes 3 parameters:
%> - <b>simulation_dir</b>: directory to save the simulation output (and where there is the .yaml file)
%> - <b>simulation_name</b>: name of the simulation (and of the .yaml file)
%> - <b>configuration_name</b>: name of the configuration to use in the .yaml file
%>
%> First it creates all the necessary objects and loads all necessary parameters by calling configuration.m.
%> Then, using a <em>try - catch</em> statement to handle possible simulation errors, it enters in a loop where it simulates the system within the different environments specified by the user.
%> \include mainForecastSimulations_loop.m
%> Considering that each simulations can have multiple environments to test, for each environment we:
%> - Simulate the system;
%> - Create a <em>.csv</em> file that contains in the name: \f$Ke\f$, \f$Je\f$, \f$settling time\f$ and \f$ts\f$;
%> - Fill the <em>.csv</em> file with the simulation output data: \f$t\f$, \f$\tau_{ref}\f$, \f$\tau\f$ and \f$err\f$;
%>
%> If enabled, after the simulation of the system in all the environments under analysis, it does two more simulations for the transparency. <br>
%> Every <em>.csv</em> file is temporarely saved in a directory specified by the user. At the end, all the <em>.csv</em> files generated are merged into one.
%> This file is the final output of the simulation software and it is the input for the evaluation software.
%>
%>
%> @section performance_indicators_evaluation Performance indicators evaluation
%> This software is written in Matlab and computes the PIs, allowing the user to evaluate data from experiments (both real and virtual).
%> The user sets the directory in which <em>.csv</em> file is stored for each experiment to evaluate.
%>
%> @subsection files_composition Files composition
%> Files composition is the following, reminding that some files are shared with the simulation software:
%> - <b>mainForecastPI.m</b>: main function to run to do the PIs evaluation
%> - <b>Utils/</b>: directory that contains some utility functions
%> - <b>configuration.m</b>: file that loads the parameters from the .yaml file
%> - <b>PerformanceIndicators/</b>: in this directory there are all the functions for the PIs
%>     - <b>static_error_index.m</b>
%>     - <b>settling_time_index.m</b>
%>     - <b>overshoot_index.m</b>
%>     - <b>mean_error_index.m</b>
%>     - <b>linearity_index.m</b>
%>     - <b>centroid_index.m</b>
%>     - <b>bandwidth_index.m</b>
%>
%> @subsection evaluation_steps Evaluation steps
%> The main function is mainForecastPI.m and it's used to compute all the PIs. It takes 3 parameters:
%> - <b>simulation_dir</b>: directory of the input files (<em>.csv</em> and <em>.yaml</em>)
%> - <b>simulation_name</b>: name of the .yaml file
%> - <b>configuration_name</b>: name of the configuration to use in the .yaml file
%>
%> First it creates all the necessary objects and loads all the necessary parameters by reading the <em>.yaml</em> file. <br>
%> Then, for each experiment in the <em>.csv</em> file, it calls the functions for every PIs.
%>
%> For each PI the outputs are:
%> - <b>PI_name.png</b>: the plot of the result of the PI in a <em>.png</em> file
%> - <b>PI_name.yaml</b>: the result of the PI in a <em>.yaml</em> file
%>
%> where <em>PI_name</em> is the name of the PI. Output files are saved in <b>pi_output/</b>.