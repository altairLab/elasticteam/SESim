% ======================================================================
%> @brief Base class for all the models implementations.
% ======================================================================
classdef IModel < handle
	
    properties (GetAccess = public, SetAccess = protected)
		%> Name of the model
        type
		%> Number of states
        num_states
		%> Number of outputs
        num_outputs
		%> Number of inputs
        num_inputs
        
		%> Spring torque signal
        tau_s
		%> Derivative of the spring torque signal
        dtau_s
    end
    
    methods
		% ======================================================================
		%> @brief Class constructor.
		%>
		%> @return instance of the IModel class.
		% ======================================================================
        function obj = IModel()
            obj.type = "TYPE";
            obj.num_states = 0;
            obj.num_outputs = 0;
            obj.num_inputs = 0;
		end
        
		% ======================================================================
		%> @brief Get the initial states.
		%> 
		%> @return x The initial states.
		% ======================================================================
        function x = get_initial_state(obj)
            x = zeros(1, obj.num_states);
		end
        
		% ======================================================================
		%> @brief Compute the derivatives for the continuous states.
		%>
		%> Method to be overridden in the child class. 
		%> 
		%> @return sys The derivatives for the continuous states.
		% ======================================================================
        function sys = fun_derivatives(obj, t, x, u)
            assert(false, "IMPLEMENT THE DERIVATIVES FUNCTION!");
            sys = zeros(obj.num_states, 1);
		end
        
		% ======================================================================
		%> @brief Compute the discrete states updates.
		%>
		%> Method to be overridden in the child class. 
		%> 
		%> @return sys The discrete states updates.
		% ======================================================================
        function sys = fun_update(obj, t, x, u)
            assert(false, "IMPLEMENT THE UPDATE FUNCTION!");
            sys = [];
		end
        
		% ======================================================================
		%> @brief Compute the outputs.
		%>
		%> Method to be overridden in the child class. 
		%> 
		%> @return sys The outputs.
		% ======================================================================
        function sys = fun_outputs(obj, t, x, u)
            assert(false, "IMPLEMENT THE OUTPUT FUNCTION!");
            sys = [];
		end
        
		% ======================================================================
		%> @brief Reset all the properties.
		% ======================================================================
        function reset(obj) 
            obj.tau_s = [];
            obj.dtau_s = [];
        end
    end
end