% ======================================================================
%> @brief Voltage controlled motor with a SEA.
% ======================================================================
classdef Voltage < IModel
	
    properties (Access = private)
		%> Processed value
        U
    end
    
    methods
		% ======================================================================
		%> @brief Class constructor.
		%>
		%> @return instance of the Voltage class.
		% ======================================================================
        function obj = Voltage()
			obj.type = "Voltage";
            obj.num_states = 5;
            obj.num_outputs = 0; %5
            obj.num_inputs = 0;
            obj.type = 'Voltage';
            
            obj.reset();
        end
        
        % ======================================================================
		%> @brief Get the initial states.
		%> 
		%> @return x The initial states.
		% ======================================================================
        function x = get_initial_state(obj)
            % TODO: theta_m, dtheta_m, q_e, dq_e
            x = [0, 0, 0, 0, 0];
        end
        
        % ======================================================================
		%> @brief Compute the derivatives for the continuous states.
		%> 
		%> @return sys The derivatives for the continuous states.
		% ======================================================================
        function sys = fun_derivatives(obj, t, x, u)
            global simulation
            params = simulation.params;
            
			% TODO: check if these are correct!
            sys(1) = x(2);
            sys(2) = (1 / params.Jm) * ( params.kt * x(5) - params.dm * x(2) - params.k_spring * (x(1) - x(3)) );
            sys(3) = x(4);
            sys(4) = (1 / params.Je) * ( params.k_spring * (x(1) - x(3)) - params.de * x(4) - params.ke * x(3) );
            sys(5) = (1 / params.L) * ( obj.U - params.R * x(5) - params.kt * x(2) );
        end
        
        % ======================================================================
		%> @brief Compute the discrete states updates.
		%> 
		%> @return sys The discrete states updates.
		% ======================================================================
        function sys = fun_update(obj, t, x, u)
            global simulation
            params = simulation.params;
            controller = simulation.controller;
            
            sys = [];
			
            % increment the index
            simulation.nextStep();
            
            % take the next ref value
            ref = simulation.ref(simulation.index);
            
            % TODO: compute tau_s as: k_s * (theta_m - q_e)
            tau_s = params.k_spring * (x(1) - x(3));
            dtau_s = params.k_spring * (x(2) - x(4));
			
            obj.U = controller.process(t, tau_s, dtau_s, x(1), x(2), x(3), x(4), ref);
        end    
        
        % ======================================================================
		%> @brief Compute the outputs.
		%> 
		%> @return sys The outputs.
		% ======================================================================
        function sys = fun_outputs(obj, t, x, u)
            sys = [];%[x(1) x(2) x(3) x(4), x(5)];
        end
        
        % ======================================================================
		%> @brief Reset all the properties.
		% ======================================================================
        function reset(obj)
			reset@IModel(obj);
			
            obj.U = 0.0;
        end
    end
end
