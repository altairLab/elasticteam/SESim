% ======================================================================
%> @brief Current controlled motor with a SEA.
%> Required parameters are: \n
%> - motor: jm, dm \n
%> - k_spring \n
%> - environment: je, de, ke
% ======================================================================
classdef Current < IModel
	
    properties (Access = private)
		%> Processed value
        U
    end
    
    methods
		% ======================================================================
		%> @brief Class constructor.
		%>
		%> @return instance of the Current class.
		% ======================================================================
        function obj = Current()
            obj.type = "Current";
            obj.num_states = 4;
            obj.num_outputs = 0;
            obj.num_inputs = 0;
            
            obj.reset();
		end
        
		% ======================================================================
		%> @brief Get the initial states.
		%> 
		%> @return x The initial states.
		% ======================================================================
        function x = get_initial_state(obj)
            % theta_m, dtheta_m, q_e, dq_e
            x = [0, 0, 0, 0];
		end
        
		% ======================================================================
		%> @brief Compute the derivatives for the continuous states.
		%> 
		%> @return sys The derivatives for the continuous states.
		% ======================================================================
        function sys = fun_derivatives(obj, t, x, u)
            global simulation;
            params = simulation.params;
			
			ks = params.motor.k_spring;
			dm = params.motor.dm;
			jm = params.motor.jm;
			ke = params.ke;
			de = params.de;
			je = params.je;
			
			% sys() is the derivative of x()
            sys(1) = x(2);
            sys(2) = (1/jm) * (obj.U - ks*(x(1)-x(3)) - dm*x(2));
            sys(3) = x(4);
            sys(4) = (1/je) * (-de*x(4) - ke*x(3) + ks*(x(1)-x(3)));
		end
        
		% ======================================================================
		%> @brief Compute the discrete states updates.
		%> 
		%> @return sys The discrete states updates.
		% ======================================================================
        function sys = fun_update(obj, t, x, u)
            global simulation;
            params = simulation.params;
            controller = simulation.controller;

			ks = params.motor.k_spring;
			
            sys = [];
            
            % increment the index
            simulation.nextStep();
            
            % take the next ref value
            ref = simulation.ref(simulation.index);
            
            % compute tau_s as: k_s * (theta_m - q_e)
            tau_s = ks * (x(1) - x(3));
            dtau_s = ks * (x(2) - x(4));
            
            obj.U = controller.process(t, tau_s, dtau_s, x(1), x(2), x(3), x(4), ref);
		end
        
		% ======================================================================
		%> @brief Compute the outputs.
		%> 
		%> @return sys The outputs.
		% ======================================================================
        function sys = fun_outputs(obj, t, x, u)
            sys = [];
		end
        
		% ======================================================================
		%> @brief Reset all the properties.
		% ======================================================================
        function reset(obj) 
            reset@IModel(obj);
            
            obj.U = 0.0;
        end
    end
end

