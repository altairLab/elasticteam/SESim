classdef CurrentTransparency < IModel
    %CURRENT TRANSPARENCY model.
    %   There is a SEA with a current controlled motor in transparency.
	%	The ref signal is used to generate the q_e signal, while the
	%	reference signal is locked to zero.
    
    properties (Access = private)
        U
		q_e
		prec_q_e
		dq_e
	end
    
    methods
        function obj = CurrentTransparency()
			obj.type = "CurrentTransparency";
            obj.num_states = 2;
            obj.num_outputs = 0;
            obj.num_inputs = 0;
		end
		
        function x = get_initial_state(obj)
            % theta_m, dtheta_m
            x = [0, 0];
        end
        
        function sys = fun_derivatives(obj, t, x, u)
            global simulation;
            params = simulation.params;
            
			ks = params.motor.k_spring;
			dm = params.motor.dm;
			jm = params.motor.jm;
			
            sys(1) = x(2);
            sys(2) = (1/jm) * (obj.U - ks*(x(1)-obj.q_e) - dm*x(2));
        end
        
        function sys = fun_update(obj, t, x, u)
            global simulation;
            params = simulation.params;
            controller = simulation.controller;

			ks = params.motor.k_spring;
			
            sys = [];
            
			% increment the index
            simulation.nextStep();
            
            % set the next ref value to zero
            ref = 0;
			
			% take next q_e value from the simulation ref signal
			obj.q_e = simulation.ref(simulation.index);
			
			% compute the derivative of q_e
			obj.dq_e = (obj.q_e - obj.prec_q_e) / simulation.params.h_physics;
			
			obj.prec_q_e = obj.q_e;
			
			% compute tau_s as: k_s * (theta_m - q_e)
            tau_s = ks * (x(1) - obj.q_e);
            dtau_s = ks * (x(2) - obj.dq_e);
			
            obj.U = controller.process(t, tau_s, dtau_s, x(1), x(2), obj.q_e, obj.dq_e, ref);
        end
        
        function sys = fun_outputs(obj, t, x, u)
            sys = [];
		end
		
		function reset(obj) 
            reset@IModel(obj);
            
			% reset all the variables
            obj.U = 0.0;
			obj.q_e = 0;
			obj.prec_q_e = 0;
			obj.dq_e = 0;
        end
    end
end

