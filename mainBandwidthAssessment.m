% RUN THIS: matlab -nodisplay -nosplash  -nodesktop -r "mainBandwidthAssessment;exit"
% This script does multiple simulations in different environments
% and then computes the bandwidth with different methods to see if they
% give similar results. 

addpath(genpath('Utils'));
addpath('PerformanceIndicators');
addpath('Simulation');
addpath('Models');
addpath('Controllers');

% remove useless temporary files
clean_sim();

clear all

% Load system parameters with user configuration
configuration;

global simulation
simulation = Simulation('sim_model.mdl');
simulation.setParams(params);
simulation.setModel(Current());

% set controller parameters
PID_ctrl = PID_Controller();
PID_ctrl.kp = 50;%15;
PID_ctrl.kd = 0.2;%0.30557;
PID_ctrl.ki = 0;

% controller
simulation.setController(PID_ctrl);

bandwidths_from_transfer_function = zeros(params.KES_NUM_ELEM,params.JES_NUM_ELEM);
bandwidths_from_pi = zeros(params.KES_NUM_ELEM,params.JES_NUM_ELEM);
bandwidths_from_sweep = zeros(params.KES_NUM_ELEM,params.JES_NUM_ELEM);
bandwidths_from_sin = zeros(params.KES_NUM_ELEM,params.JES_NUM_ELEM);

try
    for i = 1:params.KES_NUM_ELEM
        for j = 1:params.JES_NUM_ELEM
			% update simulation params
			params = simulation.params;
			params.index_ke = i;
			params.index_je = j;
			simulation.setParams(params);
            
			% PI TEST
            [bandwidths_from_pi(i,j),static_error] = bandwidthFromPi(simulation,utils)
			
            % SWEEP TEST
            bandwidths_from_sweep(i,j) = bandwidthFromSweep(simulation,static_error)
			
			% SIN TEST
%             bandwidths_from_sin(i,j) = bandwidthFromSins(simulation)
        end
	end
	
	figure();
	x = 1:params.KES_NUM_ELEM*params.JES_NUM_ELEM;
	y = [bandwidths_from_sin(:) bandwidths_from_sweep(:) ...
		bandwidths_from_pi(:) bandwidths_from_transfer_function(:)];
	bar(x,y);
	legend('from sin', 'from sweep', 'from PI', 'from tf')
	xlabel('Experiment #');
	ylabel('Bandwidth [Hz]');
	
catch e
    if isa(e,'MSLException')
        "ERROR: Simulation unstable. Change parameters and retry." + e.message
        e.getReport()
    else
        "ERROR: " + e.message
        e.getReport()
    end
end


function [bandwidth_estimated, static_error] = bandwidthFromPi(simulation,utils)
	% take parameters from simulation
	params = simulation.params;	
	
	% run the simulation with the step ref
	simulation.run(utils.unitstep,utils.duration);

	% save the simulation output
	clear simulation_output
	simulation_output.t = simulation.controller.time;
	simulation_output.ref = simulation.controller.ref;
	simulation_output.tau = simulation.controller.tau;
	simulation_output.err = simulation.controller.debug3;

	% 			% DEBUG plot
	% 			figure();
	% 			plot(simulation_output.t',[simulation_output.ref' simulation_output.tau']);

	% save the settling time of the system
	t_settling = settling_time_index(simulation_output,params,10);

	% create the reference signal
	clear simulation_output
	[ref,simulation_output.t] = reference_generator(params,t_settling);

	% run the simulation
	simulation.run(ref,max(simulation_output.t));

	% save the simulation output
	simulation_output.ref = simulation.controller.ref;
	simulation_output.tau = simulation.controller.tau;
	simulation_output.err = simulation.controller.ref - ...
		simulation.controller.tau;

	% 			% DEBUG plot
	% 			figure();
	% 			plot(simulation_output.t',[simulation_output.ref' simulation_output.tau']);

	% convert the results in a matrix
	clear matrix4pi
	matrix4pi(:,1) = simulation_output.t';
	matrix4pi(:,2) = simulation_output.ref';
	matrix4pi(:,3) = simulation_output.tau';
	matrix4pi(:,4) = simulation_output.err';

	% convert the results in a table, then save it in a .csv file
	test = array2table(matrix4pi,'VariableNames',{'t','ref','tau','err'});

	% 			% DEBUG plot
	% 			figure();
	% 			plot(test.t,[test.ref envelope(test.tau) test.tau]);

	static_error = static_error_index(test,params,t_settling);
	
	% DEBUG plot
	figure(20);
	plot(test.t,[test.ref envelope(test.tau+static_error) test.tau+static_error]);
	
	bandwidth_estimated = bandwidth_index(test,params,t_settling,static_error);
end

function bandwidth_estimated = bandwidthFromSweep(simulation,static_error)
	% take parameters from simulation
	params = simulation.params;	
	duration_sweep = params.sweep_duration;
	max_freq = params.sweep_max_freq;
	ts = simulation.params.h_control;
	t_sweep = 0:ts:duration_sweep;
	m = max_freq / duration_sweep; % m = final_value/final_time
	rect = (m/2)*t_sweep;
	sweep = 0.6 * params.sweep_amplitude + ...
		0.4 * params.sweep_amplitude * sin(2*pi*t_sweep.*rect);

	% run simulink
	simulation.run(sweep,duration_sweep);
	
	clear simulation_output
	simulation_output.t = simulation.controller.time;
	simulation_output.ref = simulation.controller.ref;
	simulation_output.tau = simulation.controller.tau;
	simulation_output.err = simulation.controller.debug3;

	%             fs = 1/simulation.params.h_physics;
	%             fc = 10;
	%             [B,A] = butter(2,fc/(fs/2),'Low'); % B num, A den
	%         test1.tau = filter(B,A,test1.tau);
	%         test1.ref = filter(B,A,test1.ref);


	%         error = abs(test1.ref - test1.tau);
	%         error = abs(2*ones(1,length(test1.ref)) - envelope(test1.tau));

	%         error = filter(B,A,error);

	t_start = ceil(length(simulation_output.tau)*0.1);
% 	static_error = 1-mean(simulation_output.tau(t_start:end-t_start))
	simulation_output.tau = simulation_output.tau + static_error;
	mm = tfest(iddata(simulation_output.tau',simulation_output.ref',0.001),2);

	% 			% DEBUG bode plot
	%             figure();
	%             h = bodeplot(mm);
	%             setoptions(h,'FreqUnits','Hz');

	%             cc = bandwidth(mm) / (2*pi)
	%             mean(test.tau(t_start:end-t_start));

	error = 1-abs(envelope(simulation_output.tau));

	if(find(abs(1-abs(error(t_start:end-t_start)))>0.293,1))
		bandwidth_estimated = t_sweep(t_start+find(abs(1-abs(error(t_start:end-t_start)))>0.293,1)) * m;
	else
		bandwidth_estimated = max_freq;
	end

	% DEBUG plot
	figure(40);
	plot(2*rect',[simulation_output.ref' envelope(simulation_output.tau)' simulation_output.tau']);
	xlabel('[Hz]');
	title(["Ke="+num2str(params.ke)+" Je="+num2str(params.je) "Sweep with freq = [0,"+num2str(max_freq)+"] Hz"]);
end

function bandwidth_estimated = bandwidthFromSins(simulation)
	% take the same max freq of the sweep from simulation
	params = simulation.params;
	MAX_FREQ = params.sweep_max_freq;
	
	% increment of the freq of the sin in each iteration
	freq_step_increment = 0.05;		% [Hz]
	
	for z = 0.1:freq_step_increment:MAX_FREQ
		% set the frequency of the sin and the bandwidth estimated
		freq = z;
		bandwidth_estimated = freq;
		
		% run simulink
		t = 0:params.h_control:params.sweep_duration;
		ref = 0.6 * params.sweep_amplitude + ...
			0.4 * params.sweep_amplitude * sin(2*pi*freq*t);
		
		% run the simulation
		simulation.run(ref,params.sweep_duration);
		
		simulation_output.t = simulation.controller.time;
		simulation_output.ref = simulation.controller.ref;
		simulation_output.tau = simulation.controller.tau;
		simulation_output.err = simulation.controller.debug3;

		% DEBUG plot
		figure();
		plot(simulation_output.t',[simulation_output.ref' simulation_output.tau']);
		
		%                 fs = 1/simulation.params.h_physics;
		%                 fc = 10;
		%                 [B,A] = butter(2,fc/(fs/2),'Low'); % B num, A den
		%         test1.tau = filter(B,A,test1.tau);
		%         test1.ref = filter(B,A,test1.ref);


		%         error = abs(test1.ref - test1.tau);
		%         error = abs(2*ones(1,length(test1.ref)) - envelope(test1.tau));

		%         error = filter(B,A,error);

		t_start = ceil(length(simulation_output.tau)*0.1);
		static_error = 1-mean(simulation_output.tau(t_start:end-t_start));
		simulation_output.tau = abs(simulation_output.tau + static_error - 1);
		simulation_output.ref = abs(simulation_output.ref - 1);
		%                 mm = tfest(iddata(test1.tau',test1.ref',0.001),2);
		%                 cc = bandwidth(mm) / (2*pi);

		error = abs(envelope(simulation_output.tau) - 1);

		%                 figure();
		%                 plot(test1.t',[test1.ref' envelope(test1.tau)' test1.tau']);
		%                 xlabel('[s]');
		%                 title(["Ke="+num2str(params.ke)+" Je="+num2str(params.je) "Sin freq = " + num2str(freq) + " Hz"]);

		if(mean(error(t_start:end))>0.293) % 1 - (1/radq(2))
			break;
		end
	end

end



