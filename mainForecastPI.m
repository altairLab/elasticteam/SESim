% RUN THIS: matlab -nodisplay -nosplash  -nodesktop -r "mainForecastPI('simulation_dir','simulation_name','configuration_name',true);exit"
%		OR	matlab -batch "mainForecastPI('simulation_dir','simulation_name','configuration_name',true);exit"
% This function reads data from a .yaml and a .csv file and computes the performance indicators.

function mainForecastPI(simulation_dir,simulation_name,configuration_name,varargin)

close all
clearvars -except simulation_dir simulation_name configuration_name varargin

addpath(genpath('Utils'));
addpath('PerformanceIndicators');

% add final slash in the simulation_dir if it doesn't exist
if(~endsWith(simulation_dir,'/'))
	simulation_dir = [simulation_dir '/'];
end

% read 'is_testbed' optional variable
nVarargs = length(varargin);
if(nVarargs==0)
    fprintf("No 'is_testbed' optional variable  --> analyzing simulated experiments\n");
elseif(nVarargs==1)
    if(varargin{1}==true)
        fprintf("'is_testbed' = true  --> analyzing testbed experiments\n");
        Unique_csv_Forecast(simulation_dir,simulation_name,configuration_name);
	else
        fprintf("'is_testbed' = false  --> analyzing simulated experiments\n");
    end
end

% load params from the .yaml file
params = Parameters(simulation_dir);
params.readFromYamlFile(simulation_name,configuration_name);

% take all .csv files under analysis
csv_name = dir([simulation_dir simulation_name '.csv']).name;
entire_table = readtable([simulation_dir csv_name],'PreserveVariableNames',true);

t = entire_table{:,1};
ref = entire_table{:,2};

kes = params.tracking.environment.k_e_vect;
jes = params.tracking.environment.j_e_vect;

MAX_KES = length(kes(:));
MAX_JES = length(jes(:));

mean_errors = zeros(MAX_KES,MAX_JES);
std_errors = zeros(MAX_KES,MAX_JES);
linearity_indexes = zeros(MAX_KES,MAX_JES);
static_error_indexes = zeros(MAX_KES,MAX_JES);
dynamic_mean_errors = zeros(MAX_KES,MAX_JES);
dynamic_std_errors = zeros(MAX_KES,MAX_JES);
overshoot_indexes = zeros(MAX_KES,MAX_JES);
high_fid_bandwidth_indexes = zeros(MAX_KES,MAX_JES);
bandwidth_indexes = zeros(MAX_KES,MAX_JES);
transparency_indexes = [];
transparency_labels = [];

index_ke = 1;
index_je = 1;
ke = kes(1);
je = jes(1);

% figure_name = [simulation.controller.name ' with kp=' num2str(simulation.controller.kp) ', ki=' num2str(simulation.controller.ki) ', kd=' num2str(simulation.controller.kd)];
% figure('Name',figure_name);
% hold on

% make the pi results directory if doesn't exist
pi_results_dir = [simulation_dir 'pi_output/'];
if(~exist(pi_results_dir, 'dir'))
	mkdir(pi_results_dir);
end

index = 1;
for i = 3:2:(MAX_KES*MAX_JES*2 + 2)
	tau = entire_table{:,i};
	err = entire_table{:,i+1};
	act_table = table(t,ref,tau,err);
	
	% figure
	fig_name = [pi_results_dir 'signal_ke=' num2str(ke) '_je=' num2str(je)];
	figure('NumberTitle','off','Name',fig_name);
	plot(act_table.t, [act_table.ref act_table.tau]);
	xlabel('Time [s]');
	ylabel('Tau [Nm]');
	legend(["Reference signal", "Tau signal"],'Location','northwest')
	saveas(gcf,[fig_name '.png']);
	
	% find and save the settling time of the system
	[step1,~,~,~] = split_signals(act_table,params);
	t_settling = settling_time_index(step1,params,10);
	
	% indexes
	[mean_errors(kes==ke,jes==je),std_errors(kes==ke,jes==je)] = mean_error_index(act_table);
	linearity_indexes(kes==ke,jes==je) = linearity_index(act_table,params);
	linearity_indexes(linearity_indexes<0) = 0;
	static_error_indexes(kes==ke,jes==je) = static_error_index(act_table,params,t_settling);
	overshoot_indexes(kes==ke,jes==je) = overshoot_index(act_table,params,t_settling);
	[bandwidth_indexes(kes==ke,jes==je),high_fid_bandwidth_indexes(kes==ke,jes==je)] = bandwidth_index(act_table,params,static_error_indexes(kes==ke,jes==je),ke,je);
	table_temp = [];
	table_temp = act_table;
	table_temp.tau = table_temp.tau + static_error_indexes(kes==ke,jes==je);
	[dynamic_mean_errors(kes==ke,jes==je),dynamic_std_errors(kes==ke,jes==je)] = mean_error_index(table_temp);
	
	index_ke = index_ke + 1;
	if(mod(index_ke,MAX_KES+1) == 0)
		index_ke = 1;
		index_je = index_je + 1;
		if(mod(index_je,MAX_JES+1) == 0)
			index_je = 1;
		end
	end
	ke = kes(index_ke);
	je = jes(index_je);

	% FOR BANDWIDTH:
	% Launch systemIdentification
	% Import time domain data
	% Estimate
	% Export the result to the workspace
	% bandwidth(tf(tf1)) / (2*pi) % this is the bandwidth of the
	% estimated transfer function
	% bode(tf1) % this is the bode plot of the estimated transfer
	% function





%     ref = interp1(simulation.controller.debt, simulation.controller.deb3, t);
%     subplot(MAX_X,MAX_Y,(i-1)*MAX_X+j);
%         figure();
%         plot(table.t, [table.ref table.tau]);
%         title(['K_e: ' num2str(ke) ' J_e: ' num2str(je)]);


% 
%     tit = title(['$' controller.name '\:\:|\:\:k_e=' num2str(params.ke) ',\:J_e=' num2str(params.Je) ',\:k_p=' num2str(controller.kp) ',\:k_d=' num2str(controller.kd) '$']);
%     tit = title(['$' 'k_e=' num2str(simulation.params.ke) ',\:J_e=' num2str(simulation.params.Je) '$']);
%     set(tit, 'Interpreter', 'Latex');
	i = i + 1;
	index = i;
end
% hold off

% transparency performance indicator
if(params.transparency.enable_transparency == true)
	with_ctrl_table = table;
	with_ctrl_table{:,1} = entire_table{:,1};
	with_ctrl_table.Properties.VariableNames{1} = 't';
	without_ctrl_table = table;
	without_ctrl_table{:,1} = entire_table{:,1};
	without_ctrl_table.Properties.VariableNames{1} = 't';
	index_with = 2;
	index_without = 2;
	for i = index+1:size(entire_table,2)
		column_name = entire_table.Properties.VariableNames{i};
		C = regexp(column_name,',transparency=with_control_','match');
		
		if(size(C,1) >= 1)
			C = regexp(column_name,',transparency=with_control_','split');
			with_ctrl_table{:,index_with} = entire_table{:,i};
			with_ctrl_table.Properties.VariableNames(index_with) = C(end);
			index_with = index_with + 1;
		else
			C = regexp(column_name,',transparency=without_control_','match');
			if(size(C,1) >= 1)
				C = regexp(column_name,',transparency=without_control_','split');
				without_ctrl_table{:,index_without} = entire_table{:,i};
				without_ctrl_table.Properties.VariableNames(index_without) = C(end);
				index_without = index_without + 1;
			else
				warning('Transparency is enabled but a column is missing.');
			end
		end
	end
	[transparency_indexes,transparency_labels] = transparencyPI(params,without_ctrl_table,with_ctrl_table);
end

% high-fidelity bandwidth index
m_err = mean(high_fid_bandwidth_indexes(:));
std_err = std(high_fid_bandwidth_indexes(:));
min_err = min(high_fid_bandwidth_indexes(:));
figure('NumberTitle','off','Name','High-Fidelity Bandwidth');
bar3(high_fid_bandwidth_indexes);
title({['Mean: ' num2str(m_err) 'Hz'], ['STD: ' num2str(std_err) 'Hz'], ['Guaranteed high-fidelity bandwidth: ' num2str(min_err) 'Hz']});
xlabel('J_e');
ylabel('K_e');
zlabel('Hz');
zlim([0 max(max(high_fid_bandwidth_indexes(:)),1)]);
set(gca,'XTickLabel',jes)
set(gca,'YTickLabel',kes)
view(45,25)
saveas(gcf,[pi_results_dir 'high_fidelity_bandwidth.png']);
% yaml file
high_fid_bandwidth_results = struct();
high_fid_bandwidth_results.type = 'vector';
high_fid_bandwidth_results.label = [{'mean'}, {'std'}, {'min'}];
high_fid_bandwidth_results.value = [m_err, std_err, min_err];
WriteYaml([pi_results_dir 'high_fidelity_bandwidth.yaml'],high_fid_bandwidth_results);

% bandwidth index
m_err = mean(bandwidth_indexes(:));
std_err = std(bandwidth_indexes(:));
min_err = min(bandwidth_indexes(:));
figure('NumberTitle','off','Name','Bandwidth');
bar3(bandwidth_indexes);
title({['Mean: ' num2str(m_err) 'Hz'], ['STD: ' num2str(std_err) 'Hz'], ['Guaranteed bandwidth: ' num2str(min_err) 'Hz']});
xlabel('J_e');
ylabel('K_e');
zlabel('Hz');
zlim([0 max(max(bandwidth_indexes(:)),1)]);
set(gca,'XTickLabel',jes)
set(gca,'YTickLabel',kes)
view(45,25)
saveas(gcf,[pi_results_dir 'bandwidth.png']);
% yaml file
bandwidth_results = struct();
bandwidth_results.type = 'vector';
bandwidth_results.label = [{'mean'}, {'std'}, {'min'}];
bandwidth_results.value = [m_err, std_err, min_err];
WriteYaml([pi_results_dir 'bandwidth.yaml'],bandwidth_results);

% MAE dynamic index
m_err = mean(dynamic_mean_errors(:));
std_err = std(dynamic_mean_errors(:));
figure('NumberTitle','off','Name','Dynamic Error');
bar3(dynamic_mean_errors);
title({['Mean: ' num2str(m_err)], ['STD: ' num2str(std_err)]});
xlabel('J_e');
ylabel('K_e');
zlabel('Dynamic error index');
zlim([0 max(dynamic_mean_errors(:))]);
set(gca,'XTickLabel',jes)
set(gca,'YTickLabel',kes)
view(45,25)
saveas(gcf,[pi_results_dir 'dynamic_error.png']);
% yaml file
dynamic_results = struct();
dynamic_results.type = 'vector';
dynamic_results.label = [{'mean'}, {'std'}];
dynamic_results.value = [m_err, std_err];
WriteYaml([pi_results_dir 'dynamic_error.yaml'],dynamic_results);

% Overshoot index
m_err = mean(overshoot_indexes(:));
std_err = std(overshoot_indexes(:));
max_err = max(overshoot_indexes(:));
figure('NumberTitle','off','Name','Overshoot Level');
bar3(overshoot_indexes);
title({['Mean: ' num2str(m_err) '%'], ['STD: ' num2str(std_err) '%'], ['Max: ' num2str(max_err) '%']});
xlabel('J_e');
ylabel('K_e');
zlabel('overshoot index %');
zlim([0 max(max_err,1)]);
set(gca,'XTickLabel',jes)
set(gca,'YTickLabel',kes)
view(45,25)
saveas(gcf,[pi_results_dir 'overshoot_level.png']);
% yaml file
overshoot_results = struct();
overshoot_results.type = 'vector';
overshoot_results.label = [{'mean'}, {'std'}, {'max'}];
overshoot_results.value = [m_err, std_err, max_err];
WriteYaml([pi_results_dir 'overshoot_level.yaml'],overshoot_results);

% Linearity index
m_err = mean(linearity_indexes(:));
figure('NumberTitle','off','Name','Linearity');
bar3(linearity_indexes);
title({['Mean: ' num2str(m_err)]});
xlabel('J_e');
ylabel('K_e');
zlabel('linearity index');
zlim([0 1]);
set(gca,'XTickLabel',jes)
set(gca,'YTickLabel',kes)
view(45,25)
saveas(gcf,[pi_results_dir 'linearity.png']);
% yaml file
linearity_results = struct();
linearity_results.type = 'scalar';
linearity_results.value = mean(linearity_indexes(:));
WriteYaml([pi_results_dir 'linearity.yaml'],linearity_results);

% Static error index
m_err = mean(static_error_indexes(:));
std_err = std(static_error_indexes(:));
figure('NumberTitle','off','Name','Static Error');
bar3(static_error_indexes);
title({['Mean: ' num2str(m_err)], ['STD: ' num2str(std_err)]});
xlabel('J_e');
ylabel('K_e');
zlabel('static error index');
zlim([0 max(static_error_indexes(:))]);
set(gca,'XTickLabel',jes)
set(gca,'YTickLabel',kes)
view(45,25)
saveas(gcf,[pi_results_dir 'static_error.png']);
% yaml file
static_results = struct();
static_results.type = 'vector';
static_results.label = [{'mean'}, {'std'}];
static_results.value = [m_err, std_err];
WriteYaml([pi_results_dir 'static_error.yaml'],static_results);

% Centroid index
[worst,best] = centroid_index(mean_errors,kes,jes);
set(gca,'XTickLabel',jes)
set(gca,'YTickLabel',kes)
view(45,25)
saveas(gcf,[pi_results_dir 'worst_case_environments.png']);
saveas(gcf,[pi_results_dir 'best_case_environments.png']);
% yaml file
worst_case_results = struct();
worst_case_results.type = 'string';
worst_case_results.value = char(worst);
WriteYaml([pi_results_dir 'worst_case_environments.yaml'],worst_case_results);
best_case_results = struct();
best_case_results.type = 'string';
best_case_results.value = char(best);
WriteYaml([pi_results_dir 'best_case_environments.yaml'],best_case_results);

% Transparency index
m_err = mean(transparency_indexes);
std_err = std(transparency_indexes);
figure('NumberTitle','off','Name','Transparency');
bar(transparency_indexes);
title({['Mean: ' num2str(m_err)], ['STD: ' num2str(std_err)]});
xlabel('Frequency [Hz]');
% xticklabels(transparency_labels)
saveas(gcf,[pi_results_dir 'transparency.png']);
% yaml file
transparency_results = struct();
transparency_results.type = 'vector';
transparency_results.label = transparency_labels;
transparency_results.value = transparency_indexes;
WriteYaml([pi_results_dir 'transparency.yaml'],transparency_results);

disp(['Done.' newline 'Figures and .yaml files available in "' pi_results_dir '".']);

end

