% ======================================================================
%> @brief Compute the BCE and WCE indexes.
%>
%> This function indicates in which environments the system works best and
%> worst.
%>
%> @param errors Mean error for each combination of the environments
%> @param kes Array of different ke environments
%> @param jes Array of different je environments
%>
%> @retval worst String pepresenting worst environments
%> @retval best String representing best environments
% ======================================================================
function [worst,best] = centroid_index(errors,kes,jes)
    
    length_kes = length(kes);
    length_jes = length(jes);
    
    matrix = zeros(2);
    ii = 1;
    jj = 1;
    
    for i = 1:length_kes
        if(i<=length_kes/2)
            ii = 1;
        elseif(mod(length_kes/2,1)==0 || ceil(length_kes/2)~=i)
            ii = 2;
        else
            continue;
        end
        
        for j = 1:length_jes
            if(j<=length_jes/2)
                jj = 1;
            elseif(mod(length_jes/2,1)==0 || ceil(length_jes/2)~=j)
                jj = 2;
            else
                continue;
            end
            
            matrix(ii,jj) = matrix(ii,jj) + errors(i,j);
        end
    end
    
    [temp,worst_env] = max(matrix);
    [temp,best_env] = min(matrix);
    
    labels = ["Low" "High"];
    
    worst = labels(worst_env(1)) + "Ke" + labels(worst_env(2)) + "Je";
    best = labels(best_env(1)) + "Ke" + labels(best_env(2)) + "Je";
    
    % plot
    figure('NumberTitle','off','Name','Best-Worst Case Environments');
    bar3(matrix);
    title(['Best: ' + best, ' Worst: ' + worst]);
    xlabel('J_e');
    ylabel('K_e');
    % legend('Best','Worst');
end