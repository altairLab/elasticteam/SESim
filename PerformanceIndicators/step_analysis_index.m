% ======================================================================
%> @brief Compute the step analysis.
%>
%> The analysis is comprehensive of: settling time, regime value,
%> delay time, rising time, and bandwidth (note that bandwidth can be not
%> accurate).
%>
%> @param test The experiment to analyze
%> @param params Parameters object
%> @param fc Cut-off frequency
%>
%> @retval res The step analysis
% ======================================================================
function res = step_analysis_index(test,params,fc)

	% take necessary variables from parameters
    fs = 1/params.h_control;
	
    % res init
    res.settling_time = test.t(end);
    res.regime_value = 0;
    res.delay_time = 0;
    res.rising_time = 0;
    res.bandwidth = 0;
%     res.oscillation_period = 0;
    
    % low pass filter 2-nd order
    [B,A] = butter(2,fc/(fs/2),'Low'); % B num, A den
    ref = test.tau;
    Y = filter(B,A,ref);
    Ydev = diff(Y)/fs;
    max_value = max(Ydev);
    for i = length(Y)-1:-1:1
        if(abs(Ydev(i)) > 0.01*max_value)
            res.settling_time = test.t(i);
            break
        end
    end
    
    if(res.settling_time == test.t(end))
        error("The system doesn't have a steady-state value.");
    end
    
%     bandwidth_TODO = bandwidth(tf(B,A,1/fs)) / (2*pi);
    
    res.regime_value = Y(end); % steady output value with time that tends to infinity
    res.delay_time = test.t(find(ref>=(0.5*res.regime_value),1)); % time needed to the output to reach the 0.5*regime_value
    t_90 = test.t(find(ref>=(0.9*res.regime_value),1)); % time where output is for the first time at 90% of the regime_value
    t_10 = test.t(find(ref>=(0.1*res.regime_value),1)); % time where output is for the first time at 10% of the regime_value
    res.rising_time = (t_90 - t_10); % time needed to the output to go from 10% to 90% of the regime_value, in seconds
    res.bandwidth = 0.35 / res.rising_time; % it should be the bandwidth, in Hz
%     res.oscillation_period = 1; % temporal distance between first two overshoots
    
%     figure();
%     s1 = subplot(2,1,1);
%     plot(test.t,[Y ref]);
%     xline(res.settling_time);
%     xline(t_90);
%     xline(t_10);
%     xlim([0 test.t(end)]);
%     title('Signal');
%     
%     s2 = subplot(2,1,2);
%     plot(test.t(1:end-1)',Ydev');
%     xline(res.settling_time);
%     xline(t_90);
%     xline(t_10);
%     yline(max_value*0.01);
%     yline(-max_value*0.01);
%     xlim([0 test.t(end-1)]);
%     title('Derivative');
%     linkaxes([s1 s2],'x');
    
%     figure();
%     h = bodeplot(tf(B,A));
%     setoptions(h,'FreqUnits','Hz');
end