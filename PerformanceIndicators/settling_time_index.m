% ======================================================================
%> @brief Compute the settling time.
%>
%> The settling time is the time needed to reach the steady state.
%>
%> @param test The experiment to analyze
%> @param params Parameters object
%> @param fc Cut-off frequency
%>
%> @retval index Settling time of the system
% ======================================================================
function index = settling_time_index(test,params,fc)

	% take necessary variables from parameters
	fs = 1/params.h_control;
	
    % low pass filter 2-nd order
    [B,A] = butter(2,fc/(fs/2),'Low'); % B num, A den
    ref = test.tau;
    Y = filter(B,A,ref);
    Ydev = diff(Y)/fs;
    index = test.t(end);
    max_value = max(Ydev);
    for i = length(Y)-1:-1:1
        if(abs(Ydev(i)) > 0.01*max_value)
            index = test.t(i);
            break
        end
    end
    
    if(index == test.t(end))
        error("The system doesn't have a steady-state value.");
    end
    
%     bandwidth_TODO = bandwidth(tf(B,A,1/fs)) / (2*pi);
    
%     figure();
%     s1 = subplot(2,1,1);
%     plot(test.t',[Y' ref']);
%     xline(index);
%     xlim([0 test.t(end)]);
%     title('Signal');
%     
%     s2 = subplot(2,1,2);
%     plot(test.t(1:end-1)',Ydev');
%     xline(index);
%     yline(max_value*0.01);
%     yline(-max_value*0.01);
%     xlim([0 test.t(end-1)]);
%     title('Derivative');
%     linkaxes([s1 s2],'x');
    
%     figure();
%     bode(tf(B,A));
end