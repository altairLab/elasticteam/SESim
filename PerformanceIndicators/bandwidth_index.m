% ======================================================================
%> @brief Compute the bandwidth index.
%>
%> The bandwidth index is a slightly different bandwidth of the system.
%>
%> @param test The experiment to analyze
%> @param params Parameters object
%> @param static_error Static error of the system
%> @param ke Stiffness of the environment under analysis
%> @param je Inertia of the environment under analysis
%>
%> @retval normal_band Bandwidth
%> @retval high_fid_band High-fidelity bandwidth
% ======================================================================
function [normal_band,high_fid_band] = bandwidth_index(test,params,static_error,ke,je)
    
	% take necessary variables from parameters
    sweep_max_freq = params.tracking.reference_signal.sweep_max_freq;
    sweep_duration = params.tracking.reference_signal.sweep_duration;
	sweep_offset = params.tracking.reference_signal.amplitude_step_4;
    sweep_amplitude = params.tracking.reference_signal.sweep_amplitude;
	
    [step1,~,~,final_sweep] = split_signals(test,params);
	
	% bandwidth
	step_analysis_res = step_analysis_index(step1,params,10);
	normal_band = step_analysis_res.bandwidth;
	
    % time start to 0
    final_sweep.t = final_sweep.t - min(final_sweep.t);
    
    % remove first samples
    t_start = ceil(length(final_sweep.t)*0.1);
    
    % angular coefficient of the rect
    m = sweep_max_freq / sweep_duration;
    
% 	figure('NumberTitle','off','Name',['Bandwith analysis of Ke=' num2str(ke) ',Je=' num2str(je)])
% 	subplot(3,1,3)
% 	plot(final_sweep.t*m,[final_sweep.ref,final_sweep.tau]);
	
    % add static error to tau
    final_sweep.tau = abs(final_sweep.tau + static_error - sweep_offset);
	final_sweep.ref = abs(final_sweep.ref - sweep_offset);
    
    % calculate error between the envelope of the sweep and of tau
%     error = abs(sweep_offset+sweep_amplitude-envelope(final_sweep.tau))/(sweep_offset+sweep_amplitude);
	error = (abs(final_sweep.ref - final_sweep.tau)) / (sweep_offset);
	
	if(find(error(t_start:end)>0.293,1))
		if(find(error(t_start:end)>0.293,1)+t_start <= size(final_sweep.t,1))
			high_fid_band = final_sweep.t(find(error(t_start:end)>0.293,1)+t_start) * m;
		else
			high_fid_band = sweep_max_freq;
		end
	else
		high_fid_band = sweep_max_freq;
	end
	
% 	subplot(3,1,1)
% 	plot(final_sweep.t*m,error)
% 	hold on
% 	xline(index,'-','LineWidth',2);
% 	hold off
% 	subplot(3,1,2)
% 	plot(final_sweep.t*m,[final_sweep.ref,final_sweep.tau]);
% 	hold on
% 	xline(index,'-','LineWidth',2);
% 	hold off
end
