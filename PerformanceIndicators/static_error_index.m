% ======================================================================
%> @brief Compute the static error index.
%>
%> The static error index is a value (minimum 0) representing the static
%> error of the system. Lower value is better.
%>
%> @param test The experiment to analyze
%> @param params Parameters object
%> @param settling_time Settling time of the system
%>
%> @retval index The static error index
% ======================================================================
function index = static_error_index(test,params,settling_time)

    % take necessary variables from parameters
	ts = params.h_control;
	step_duration = params.tracking.reference_signal.step_duration;
    
	% step 1
    t1 = fix((settling_time/ts)+1);
    t2 = fix(step_duration/ts);
    static_test = test(t1:t2,:);
	
	% step 2
    t1 = fix(t2 + (settling_time/ts)+1);
    t2 = fix(2*step_duration/ts);
    static_test = vertcat(static_test,test(t1:t2,:));
	
	% step 3
    t1 = fix(t2 + (settling_time/ts)+1);
    t2 = fix(3*step_duration/ts);
    static_test = vertcat(static_test,test(t1:t2,:));
    
    index = calculate_static_error_index(static_test);
end

% ======================================================================
%> @brief Support function for the static error index. 
%> 
%> Don't use this function directly.
%> 
%> @param test The experiment to analyze
%>
%> @retval index The static error index
% ======================================================================
function index = calculate_static_error_index(test)
    % STATIC ERROR INDEX is a value where the minimum value is 0.
    % A lower value means that the system has low static error.
    
    err = abs(test.ref - test.tau);
% 	TODO: NORMALIZZARE
    % calculating the Mean Absolute Error (MAE)
    index = sum(err)/length(err);
end