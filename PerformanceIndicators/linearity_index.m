% ======================================================================
%> @brief Compute the linearity index.
%>
%> The linearity index is a value included in [0,1]. It represents the
%> linearity of the system, where a value of 1 means that the system
%> is linear.
%>
%> @param test The experiment to analyze
%> @param params Parameters object
%>
%> @retval index The linearity index
% ======================================================================
function index = linearity_index(test,params)
    
	% take necessary variables from parameters
    amplitude_step_1 = params.tracking.reference_signal.amplitude_step_1;
    amplitude_step_2 = params.tracking.reference_signal.amplitude_step_2;
    amplitude_step_3 = params.tracking.reference_signal.amplitude_step_3;
    
	% take steps
	[step1,step2,step3,~] = split_signals(test,params);
	
	% steps normalized
    tau1_norm = step1.tau / amplitude_step_1;
    tau2_norm = step2.tau / amplitude_step_2;
    tau3_norm = step3.tau / amplitude_step_3;
    
	% tau1 and tau2
    indexes(1) = mean_abs_error(tau1_norm,tau2_norm);
	
	% tau1 and tau3
    indexes(2) = mean_abs_error(tau1_norm,tau3_norm);
	
	% tau2 and tau3
    indexes(3) = mean_abs_error(tau2_norm,tau3_norm);
	
	index = 1 - mean(indexes);
end

% ======================================================================
%> @brief Support function for the linearity index.
%>
%> Don't use this function directly.
%>
%> @param taumax Tau of step 1
%> @param taumin Tau of step 2
%>
%> @retval res The mean absolute error
% ======================================================================
function res = mean_abs_error(tau1,tau2)
    err = abs(tau1 - tau2);
    res = mean(err);
end