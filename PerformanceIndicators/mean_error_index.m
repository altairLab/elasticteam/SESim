% ======================================================================
%> @brief Compute the mean error.
%>
%> The mean is computed on the absolute error between reference 
%> and tau signals.
%>
%> @param test The experiment to analyze
%>
%> @retval mean_index The mean error
%> @retval std_index The standard variation of the error
% ======================================================================
function [mean_index,std_index] = mean_error_index(test)

    err = abs(test.ref - test.tau);

    % calculating the Mean Absolute Error (MAE)
    mean_index = sum(err)/length(err);
    std_index = std(err);
end