% ======================================================================
%> @brief Compute the overshoot index.
%>
%> The overshoot index is the higher overshoot (in percentage).
%>
%> @param test The experiment to analyze
%> @param params Parameters object
%> @param settling_time Settling time of the system
%>
%> @retval index The overshoot index
% ======================================================================
function index = overshoot_index(test,params,settling_time)

	% take necessary variables from parameters
    ts = params.h_control;
	step_duration = params.tracking.reference_signal.step_duration;
    
	[step1,step2,step3,~] = split_signals(test,params);
	steps = {step1,step2,step3};
	index = 0;
	
	for i = 1:length(steps)
		tau = steps{i}.tau;
		max_value = max(tau(1:fix((settling_time/ts))));
		steady_value = mean(tau(fix((settling_time/ts)+1):fix(step_duration/ts)-1));
		
		if(max_value >= steady_value)
			overshoot = 100 * ((max_value - steady_value) / steady_value);
			if(overshoot > index)
				index = overshoot;
			end
		end
	end
	
end
