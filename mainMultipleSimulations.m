addpath('Utils');
addpath('Simulation');
addpath('Models');
addpath('Controllers');

clean_sim();

clear all

% Load system parameters with user configuration
configuration;

simulation_dir = params.simulation_output_dir;
simulation_name = 'simulation1';

global simulation
simulation = Simulation('sim_model.mdl');
simulation.setParams(params);
simulation.setModel(Current());

% Set controller parameters
PID_ctrl = PID_Controller();
PID_ctrl.kp = 15;
PID_ctrl.kd = 0.30557;
PID_ctrl.ki = 0;

% controller
simulation.setController(PID_ctrl);

mean_errors = zeros(params.KES_NUM_ELEM,params.JES_NUM_ELEM);

try
    for i = 1:params.KES_NUM_ELEM
        for j= 1:params.JES_NUM_ELEM
            
            % update params
            params = simulation.params;
            params.index_ke = i;
			params.index_je = j;

            % set params, model and reset the controller
            simulation.setParams(params);
            
			% run the simulation with the step ref
			simulation.run(utils.unitstep,utils.duration);
			
            % take the results
            res.t = simulation.controller.time;
            res.ref = simulation.controller.ref;
            res.tau = simulation.controller.tau;
            res.err = simulation.controller.debug3;

            % calculating the Mean Absolute Error (MAE)
            mean_errors(i,j) = sum(res.err)/length(res.err);
			
            figure();
            plot(res.t',[res.ref' res.tau' res.err']);
        end
    end
catch e
    if isa(e,'MSLException')
        "ERROR: Simulation unstable. Change parameters and retry." + e.message
    else
        "ERROR: " + e.message
        e.getReport()
    end
end

figure('NumberTitle','off','Name','Errors');
bar3(mean_errors);
title('Errors');
xlabel('J_e');
ylabel('K_e');
zlabel('Error');
if (min(mean_errors(:)) ~= max(mean_errors(:)))
    zlim([min(mean_errors(:)) max(mean_errors(:))]);
end
set(gca,'XTickLabel',params.jes)
set(gca,'YTickLabel',params.kes)
