
clear all;
clc


addpath(genpath('Utils'));
addpath('PerformanceIndicators');
addpath('Simulation');
addpath('Models');
addpath('Controllers');




% load configuration
configuration;


try
    % testing all environments 
%     mainForecastSimulations;
    
    
    % executing the transparency PI 
    
    mainTransparency;
   

catch e
    if isa(e,'MSLException')
        "ERROR: Simulation unstable. Change parameters and retry." + e.message
		e.getReport()
    else
        "ERROR: " + e.message
        e.getReport()
    end
end
