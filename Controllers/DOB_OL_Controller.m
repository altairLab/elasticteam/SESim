% ======================================================================
%> @brief Implementation of a DOB OL controller.
% ======================================================================
classdef DOB_OL_Controller < IController
    properties (Access = public)
        %> Feed forward
        FF 
        %> Proportional gain
        kp 
        %> Derivative gain
        kd
    end
    properties (GetAccess = private, SetAccess = public)
        % filters variables
        parameters
        order_QPn
        a_QPn
        b_QPn
        order_Q
        a_Q
        b_Q 
    end
    properties (Access = private)
        filterQPn
        filterQ
        prev_Ud
    end
    
    methods
        % ======================================================================
		%> @brief Class constructor.
		%>
		%> @return instance of the DOB_OL_Controller class.
		% ======================================================================
        function obj = DOB_OL_Controller()
            obj.name = 'DOB_OL';
            obj.reset();
        end
        
        % ======================================================================
		%> @brief The control law of the controller.
		%> 
		%> @param t Time value for the actual step
		%> @param tau_s Spring torque value for the actual step
		%> @param dtau_s Derivative of the spring torque for the actual step
		%> @param theta_m Motor position for the actual step
		%> @param dtheta_m Derivative of the motor position for the actual step
		%> @param theta_e Environment position for the actual step
		%> @param dtheta_e Derivative of the environment position for the actual step
		%> @param ref Reference value for the actual step
		%>
		%> @return U The processed value.
		% ======================================================================
        function U = process_(obj, t, tau_s, dtau_s, theta_m, dtheta_m, theta_e, dtheta_e, ref)
            % necessary to retrieve the index
            global simulation;
            index = simulation.index;
            
            dref = 0;
            if(t-obj.prev_t ~= 0)
                dref = (ref - obj.prev_ref) / (t - obj.prev_t);
            end

            err = ref - tau_s;
            derr = dref - dtau_s;
            
            Up = obj.kp * err + obj.kd * derr + ref * obj.FF;
            
            D1 = obj.filterQ.process(obj.prev_Ud, t);
            D2 = obj.filterQPn.process(tau_s, t);
            
            d_hat = D2 - D1;
            U = Up - d_hat;
            
            obj.prev_Ud = U;
            
            % Debug
            obj.debug1(index) = Up;
            obj.debug2(index) = dref;
            obj.debug3(index) = abs(err);
        end
        
        % ======================================================================
		%> @brief Reset all the properties.
		% ======================================================================
        function reset(obj) 
            % Reset all parameters here
            reset@IController(obj);
            
            obj.prev_Ud = 0.0;
            
            global simulation;
			params = simulation.params;
            
            obj.filterQ = FilterNthOrder(obj.order_Q, obj.a_Q, obj.b_Q, params.h_physics);
            obj.filterQPn = FilterNthOrder(obj.order_QPn, obj.a_QPn, obj.b_QPn, params.h_physics);
        end
    end
end