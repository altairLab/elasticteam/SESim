% ======================================================================
%> @brief Implementation of a PID controller.
%> Required parameters are: \n
%> - kp \n
%> - ki \n
%> - kd
% ======================================================================
classdef PID_Controller < IController
	
    properties (Access = public)
		%> Proportional gain
        kp
		%> Derivative gain
        kd
		%> Integrative gain
        ki
		
		%> Integrative error
        integrative_error
    end
    
    methods
		% ======================================================================
		%> @brief Class constructor.
		%>
		%> @return instance of the PID_Controller class.
		% ======================================================================
        function obj = PID_Controller()
            obj.name = 'PID_Controller';
            obj.kp = 0;
            obj.kd = 0;
            obj.ki = 0;
            obj.reset();
		end
        
		% ======================================================================
		%> @brief The control law of the controller.
		%> 
		%> @param t Time value for the actual step
		%> @param tau_s Spring torque value for the actual step
		%> @param dtau_s Derivative of the spring torque for the actual step
		%> @param theta_m Motor position for the actual step
		%> @param dtheta_m Derivative of the motor position for the actual step
		%> @param theta_e Environment position for the actual step
		%> @param dtheta_e Derivative of the environment position for the actual step
		%> @param ref Reference value for the actual step
		%>
		%> @return U The processed value.
		% ======================================================================
        function U = process_(obj, t, tau_s, dtau_s, theta_m, dtheta_m, theta_e, dtheta_e, ref)
            % necessary to retrieve the index
            global simulation;
            index = simulation.index;
			params = simulation.params;
            
            dref = 0;
            if(t-obj.prev_t ~= 0)
                dref = (ref - obj.prev_ref) / (t - obj.prev_t);
            end
            
            % compute the three different errors
            err = ref - tau_s;
            derr = dref - dtau_s;
            obj.integrative_error = obj.integrative_error + err;

            % PID law
            U = obj.kp * err + obj.kd * derr + obj.ki * obj.integrative_error;
            
			% current saturation (only if it's positive)
			if(params.motor.current_saturation>0 && abs(U)>params.motor.current_saturation)
				U = sign(U) * params.motor.current_saturation;
			end
			
            % Debug
            obj.debug1(index) = U;
            obj.debug2(index) = dref;
            obj.debug3(index) = abs(err);
		end
        
		% ======================================================================
		%> @brief Reset all the properties.
		% ======================================================================
        function reset(obj) 
            % Reset all parameters
            reset@IController(obj);
            
            global simulation;
			params = simulation.params;
            obj.kp = params.motor_controller.kp;
            obj.kd = params.motor_controller.kd;
            obj.ki = params.motor_controller.ki;
			
            % reset the integrative error
            obj.integrative_error = 0.0;
        end
    end
end

