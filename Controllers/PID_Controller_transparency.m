% ======================================================================
%> @brief Implementation of a PID controller.
%> Required parameters are: \n
%> - kp \n
%> - ki \n
%> - kd
% ======================================================================
classdef PID_Controller_transparency < IController
	
    properties (Access = public)
		%> Proportional gain
        kp
		%> Derivative gain
        kd
		%> Integrative gain
        ki
		
		%> Integrative error
        integrative_error
        
        % estimation variables
        theta_e
        theta_m
        dtheta_m
        ddtheta_m
        tau_s
        tau_m
        prev_dtheta_m
        
        % control variable
        enable_output
    end
    
    methods
		% ======================================================================
		%> @brief Class constructor.
		%>
		%> @return instance of the PID_Controller class.
		% ======================================================================
        function obj = PID_Controller_transparency()
            obj.name = 'PID_Controller_transparency';
            obj.kp = 0;
            obj.kd = 0;
            obj.ki = 0;
            obj.theta_m = [];
            obj.dtheta_m = [];
            obj.ddtheta_m = [];
            obj.prev_dtheta_m = 0.0;
            obj.enable_output = true;
            obj.theta_e = [];
 
            obj.reset();
		end
        
		% ======================================================================
		%> @brief The control law of the controller.
		%> 
		%> @param t Time value for the actual step
		%> @param tau_s Spring torque value for the actual step
		%> @param dtau_s Derivative of the spring torque for the actual step
		%> @param theta_m Motor position for the actual step
		%> @param dtheta_m Derivative of the motor position for the actual step
		%> @param theta_e Environment position for the actual step
		%> @param dtheta_e Derivative of the environment position for the actual step
		%> @param ref Reference value for the actual step
		%>
		%> @return U The processed value.
		% ======================================================================
        function U = process_(obj, t, tau_s, dtau_s, theta_m, dtheta_m, theta_e, dtheta_e, ref)
            % necessary to retrieve the index
            global simulation;
            index = simulation.index;
			params = simulation.params;
			
            dref = 0;
            if(t>0)
                dref = (ref - obj.prev_ref) / (t - obj.prev_t);
            end
            
            % compute the three different errors
            err = - tau_s;
            derr = - dtau_s;
            obj.integrative_error = obj.integrative_error + err;

            % PID law
            U = obj.kp * err + obj.kd * derr + obj.ki * obj.integrative_error;
            
			% current saturation (only if it's positive)
			if(params.motor.current_saturation>0 && abs(U)>params.motor.current_saturation)
				U = sign(U) * params.motor.current_saturation;
			end
            if ~ obj.enable_output
             U = 0;
             
            end
%             U=0;
            
            obj.theta_m(index) = theta_m;
			obj.dtheta_m(index) = dtheta_m;
            obj.ddtheta_m(index) = (dtheta_m-obj.prev_dtheta_m)/(t - obj.prev_t);
            obj.tau_s(index) = tau_s;
            obj.tau_m(index) = U;
            obj.prev_dtheta_m = dtheta_m;
            obj.theta_e(index) = theta_e;
            
            % Debug
            obj.debug1(index) = U;
            obj.debug2(index) = dtheta_e;
            obj.debug3(index) = abs(err);
		end
        
		% ======================================================================
		%> @brief Reset all the properties.
		% ======================================================================
        function reset(obj) 
            % Reset all parameters
            reset@IController(obj);
            obj.tau_s = [];
            obj.tau_m = [];
			
			% hard-coded parameters
            obj.kp = 1;
            obj.kd = 0;
            obj.ki = 0;
            
            % reset the integrative error
            obj.integrative_error = 0.0;
        end
    end
end

