classdef DDOB_Controller < IController
    %DDOB_CONTROLLER Summary of this class goes here
    %   Detailed explanation goes here
   
    properties (Access = public)
        FF 
        kp 
        kd
    end
    properties (GetAccess = private, SetAccess = public)
        parameters
        order_QPn
        a_QPn
        b_QPn
        order_Q
        a_Q
        b_Q 
    end
    properties (Access = private)
        filterQPn
        filterQ
        prev_Ud
    end
    
    methods
        function obj = DDOB_Controller()
            obj.name = 'dDOB';
            obj.prev_t = 0.0;
            obj.prev_Ud = 0.0;
            obj.prev_ref = 0.0;
            obj.debi = 1;
        end
        
        function U = process(obj, t, theta_m, dtheta_m, theta_e, dtheta_e, ref)
            
            if ~ exist('ref','var')
                % % STEP
                ref = 0.1*sign(sin(2*pi*0.5*t));
                % % SIN
                % r = sin(2*pi*0.5*t);
                % % SWIPE
                % freq = (t / duration) * 5.0;
                % r = - 0.7 * (0.4 + 0.25 * (sin(2 * pi * freq * t)));
                dref = 0;
            else
                dref = (ref - obj.prev_ref) * (t - obj.prev_t);
            end

            if obj.prev_t == 0
                obj.filterQ = FilterNthOrder(obj.order_Q, obj.a_Q, obj.b_Q, obj.parameters.h);
                obj.filterQPn = FilterNthOrder(obj.order_QPn, obj.a_QPn, obj.b_QPn, obj.parameters.h);
            end

            % Force control
            tau = obj.parameters.k_spring * (theta_m - theta_e);
            dtau = obj.parameters.k_spring * (dtheta_m - dtheta_e);

            err = ref - tau;

            Up = obj.kp * err + ref * obj.FF; % External P and FeedForward action

%             D1 = filter_nth_order(obj.order_Q, obj.prev_Ud, obj.a_Q, obj.b_Q, t);
%             D2 = filter_nth_order(obj.order_QPn, tau, obj.a_QPn, obj.b_QPn, t);
            D1 = obj.filterQ.process(obj.prev_Ud, t);
            D2 = obj.filterQPn.process(tau, t);

            d_hat = D2 - D1;
            % d_hat = 0;

            Ud = Up - d_hat; % DOB action

            U = Ud - obj.kd * dtau; % innerD action
            
            obj.prev_ref = ref;
            obj.prev_Ud = Ud;
            obj.prev_t = t;

            % Debug
            obj.debi = obj.debi + 1;
            obj.debt(obj.debi) = t;
            obj.deb1(obj.debi) = ref;
            obj.deb2(obj.debi) = dref;
            obj.deb3(obj.debi) = d_hat;
        end
        
        function reset(obj) 
            % Reset all parameters here
            obj.prev_Ud = 0.0;
            
            reset@IController(obj);
        end
    end
end

