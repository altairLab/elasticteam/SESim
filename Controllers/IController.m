% ======================================================================
%> @brief Base class for all the controllers implementations.
% ======================================================================
classdef IController < handle
	
    properties (GetAccess = public, SetAccess = private)
		%> Time signal
        time 
		%> Reference signal
        ref
		%> Tau signal
        tau
		
		%> Processed value in the previous step
		prev_U
		%> Reference value in the previous step
        prev_ref
		%> Time value in the previous step
        prev_t
    end
    properties (GetAccess = public, SetAccess = protected)
		%> Name of the controller
        name
		
        %> General purpose debug signal
        debug1
		%> General purpose debug signal
        debug2 
		%> General purpose debug signal
        debug3
	end
	properties (Access = public)
		%> Variable used to manage the control loop frequency
		control_loop_max
		%> Variable used to manage the control loop frequency
		control_loop_count
	end
    
    methods (Access = public)
		% ======================================================================
		%> @brief Class constructor.
		%>
		%> @return instance of the IController class.
		% ======================================================================
        function obj = IController()
            obj.name = 'NAME';
            obj.reset();
		end

		% ======================================================================
		%> @brief The control law of the controller.
		%>
		%> Method to be overridden in the child class. 
		%> 
		%> @param t Time value for the actual step
		%> @param tau_s Spring torque value for the actual step
		%> @param dtau_s Derivative of the spring torque for the actual step
		%> @param theta_m Motor position for the actual step
		%> @param dtheta_m Derivative of the motor position for the actual step
		%> @param theta_e Environment position for the actual step
		%> @param dtheta_e Derivative of the environment position for the actual step
		%> @param ref Reference value for the actual step
		%>
		%> @return U The processed value.
		% ======================================================================
        function U = process_(obj, t, tau_s, dtau_s, theta_m, dtheta_m, theta_e, dtheta_e, ref)
            assert(false, "IMPLEMENT THE CONTROLLER!");
            U = 0;
		end
		
        % ======================================================================
		%> @brief Reset all the properties.
		% ======================================================================
        function reset(obj) 
			obj.prev_U = 0.0;
            obj.prev_ref = 0.0;
            obj.prev_t = 0.0;
            
            obj.time = [];
            obj.debug1 = [];
            obj.debug2 = [];
            obj.debug3 = [];
            
            obj.tau = [];
            obj.ref = [];
			
			obj.setControlLoop();
		end
	end
    methods (Access = public, Sealed = true)
		% ======================================================================
		%> @brief Manage the control loop frequency and call the child
		%> class process_() to exec the control law.
		%>
		%> @param t Time value for the actual step
		%> @param tau_s Spring torque value for the actual step
		%> @param dtau_s Derivative of the spring torque for the actual step
		%> @param theta_m Motor position for the actual step
		%> @param dtheta_m Derivative of the motor position for the actual step
		%> @param theta_e Environment position for the actual step
		%> @param dtheta_e Derivative of the environment position for the actual step
		%> @param ref Reference value for the actual step
		%>
		%> @return U The processed value.
		% ======================================================================
        function U = process(obj, t, tau_s, dtau_s, theta_m, dtheta_m, theta_e, dtheta_e, ref)
            % necessary to retrieve the index
            global simulation
            index = simulation.index;
			
            U = obj.prev_U;
			
			% enter in this loop only with a frequency of 1/h_control
			obj.control_loop_count = obj.control_loop_count + 1;
			if(obj.control_loop_count >= obj.control_loop_max)
				obj.control_loop_count = 0;
				
				% call the process of the child
				U = obj.process_(t,tau_s,dtau_s,theta_m,dtheta_m,theta_e,dtheta_e,ref);
				
				% update previous controller values
				obj.prev_ref = ref;
				obj.prev_t = t;
				obj.prev_U = U;
			else
				% update arrays with previous values
				obj.debug1(index) = obj.debug1(index-1);
				obj.debug2(index) = obj.debug2(index-1);
				obj.debug3(index) = obj.debug3(index-1);
			end
            
            % update arrays with new values
            obj.time(index) = t;
            obj.ref(index) = ref;
            obj.tau(index) = tau_s;
        end
    end
	methods (Access = private, Sealed = true)
		% ======================================================================
		%> @brief Set the control loop frequency and show warning dialogs
		%> if something went wrong.
		% ======================================================================
		function setControlLoop(obj)
			global simulation;
			h_control = simulation.params.h_control;
			h_physics = simulation.params.h_physics;
			
			if(h_control < h_physics)
				opts = struct('WindowStyle','modal', ...
					'Interpreter','tex');
				f = warndlg({['\ith\_control \rm' ...
					'must be equal of greater than h\_physics.'] ...
					'\color{red}Set it in the configuration.m file\color{black}.' ...
					['For these simulations it will be set equal to ' ...
					'h\_physics (' num2str(h_physics*obj.control_loop_max) ...
					num2str(h_physics) 's).']}, ...
					'Warning: h_control not set properly', opts);
				waitfor(f);
				
				simulation.params.h_control = h_physics;
				h_control = h_physics;
			end
			
			obj.control_loop_max = 0;
			while(h_control>0)
				h_control = h_control - h_physics;
				if(h_control>-1e-10 && h_control<1e-10)
					h_control = 0;
				end
				obj.control_loop_max = obj.control_loop_max + 1;
			end
			
			if(h_control ~= 0)
				disp(h_control)
				opts = struct('WindowStyle','modal', ...
					'Interpreter','tex');
				f = warndlg({['\ith\_control \rm' ...
					'must be a multiple of h\_physics.'] ...
					'\color{red}Set it in the configuration.m file\color{black}.' ...
					['For these simulations it will be set equal to ' ...
					num2str(h_physics*obj.control_loop_max) 's.']}, ...
					'Warning: h_control not set properly', opts);
				waitfor(f);
				
				simulation.params.h_control = h_physics*obj.control_loop_max;
			end
			
			obj.control_loop_count = obj.control_loop_max;
		end
    end
end

