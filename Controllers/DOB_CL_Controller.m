% ======================================================================
%> @brief Implementation of a DOB CL controller.
% ======================================================================
classdef DOB_CL_Controller < IController
    properties (Access = public)
        %> Feed forward
        FF 
        %> Proportional gain
        kp 
        %> Derivative gain
        kd
    end
    properties (GetAccess = private, SetAccess = public)
        % filters variables
        parameters
        order_QPn
        a_QPn
        b_QPn
        order_Q
        a_Q
        b_Q 
    end
    properties (Access = private)
        filterQPn
        filterQ
        prev_rd
        prev_d_hat
    end
    
    methods
        % ======================================================================
		%> @brief Class constructor.
		%>
		%> @return instance of the DOB_CL_Controller class.
		% ======================================================================
        function obj = DOB_CL_Controller()
            obj.name = 'DOB_CL';
            obj.reset();
        end
        
        % ======================================================================
		%> @brief The control law of the controller.
		%> 
		%> @param t Time value for the actual step
		%> @param tau_s Spring torque value for the actual step
		%> @param dtau_s Derivative of the spring torque for the actual step
		%> @param theta_m Motor position for the actual step
		%> @param dtheta_m Derivative of the motor position for the actual step
		%> @param theta_e Environment position for the actual step
		%> @param dtheta_e Derivative of the environment position for the actual step
		%> @param ref Reference value for the actual step
		%>
		%> @return U The processed value.
		% ======================================================================
        function U = process_(obj, t, tau_s, dtau_s, theta_m, dtheta_m, theta_e, dtheta_e, ref)
            % necessary to retrieve the index
            global simulation;
            index = simulation.index;
            
            rd = ref - obj.prev_d_hat;
            
            drd = 0;
            if(t ~= 0)
                drd = (rd - obj.prev_rd) / (t - obj.prev_t);
            end
            
            err = rd - tau_s;
            derr = drd - dtau_s;
            
            U = obj.kp * err + obj.kd * derr + rd * obj.FF;

            D1 = obj.filterQ.process(rd, t);
            D2 = obj.filterQPn.process(tau_s, t);
            d_hat = D2 - D1;

            obj.prev_d_hat = d_hat;
            obj.prev_rd = rd;
           
            % Debug
            obj.debug1(index) = rd;
            obj.debug2(index) = d_hat;
            obj.debug3(index) = D2;
        end
        
        % ======================================================================
		%> @brief Reset all the properties.
		% ======================================================================
        function reset(obj) 
            % Reset all parameters here
            reset@IController(obj);
            
            obj.prev_d_hat = 0.0;
            obj.prev_rd = 0.0;
            
            global simulation;
			params = simulation.params;
            
            obj.filterQ = FilterNthOrder(obj.order_Q, obj.a_Q, obj.b_Q, params.h_physics);
            obj.filterQPn = FilterNthOrder(obj.order_QPn, obj.a_QPn, obj.b_QPn, params.h_physics); 
        end
    end
end

