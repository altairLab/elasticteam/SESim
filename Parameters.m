% ======================================================================
%> @brief Class that handles the parameters for: 
%> simulation, motor, controller, reference signal and environment
% ======================================================================
classdef Parameters < dynamicprops

    properties (GetAccess = public, SetAccess = public)
        %> simulation parameter - physics update period
        h_physics   
        %> simulation parameter - control update period
        h_control
        
		%> motor parameters
		motor
		%> motor controller parameters
		motor_controller
		
		%> transparency parameters
		transparency
		%> tracking parameters
		tracking
	end
	properties (GetAccess = public, SetAccess = private)
		%> directory of the simulation output
		simulation_output_dir

		%> configuration name
		config_name
	end

    methods
		% ======================================================================
		%> @brief Class constructor
		%>
		%> @return instance of the Parameters class.
		% ======================================================================
        function obj = Parameters(simulation_output_dir)
			% simulation parameters
            obj.h_physics = 1e-3;
            obj.h_control = 1e-3;
			obj.config_name = 'conf1';
            
			% motor parameters
			obj.motor = struct();
			obj.motor.motor_type = 'Current';
			obj.motor.k_spring = 12.34;
			obj.motor.current_saturation = 1;
            obj.motor.jm = 0.02;
            obj.motor.dm = 0.01;
			
			% motor controller parameters
			obj.motor_controller = struct();
			
			% transparency parameters
			obj.transparency = struct();
			obj.transparency.enable_transparency = false;
			obj.transparency.environment = struct();
			obj.transparency.environment.k_des = 0;
			obj.transparency.environment.b_des = 0;
			obj.transparency.environment.j_des = 0;
			obj.transparency.reference_signal = struct();
			obj.transparency.reference_signal.sweep_max_freq = 10;
			obj.transparency.reference_signal.sweep_amplitude = 0.001;
			obj.transparency.reference_signal.sweep_duration = 10;
            
			% tracking parameters
			obj.tracking = struct();
			obj.tracking.environment = struct();
			obj.tracking.environment.damping_factor = 1.0;
			obj.tracking.environment.index_ke = 1;
			obj.tracking.environment.index_je = 1;
			obj.tracking.environment.k_e_vect = [0.01, 0.02];
			obj.tracking.environment.j_e_vect = [0.001, 0.002];
			obj.tracking.environment.KES_NUM_ELEM = 2;
			obj.tracking.environment.JES_NUM_ELEM = 2;
			obj.tracking.reference_signal = struct();
			obj.tracking.reference_signal.amplitude_step_1 = 0.5;
			obj.tracking.reference_signal.amplitude_step_2 = 1.0;
			obj.tracking.reference_signal.amplitude_step_3 = 1.5;
			obj.tracking.reference_signal.amplitude_step_4 = 1.0;
			obj.tracking.reference_signal.step_duration = 10;
			obj.tracking.reference_signal.sweep_max_freq = 5;
			obj.tracking.reference_signal.sweep_amplitude = 0.4;
			obj.tracking.reference_signal.sweep_duration = 10;
			
			% make the directory of simulation outputs if it doesn't exist
			obj.simulation_output_dir = simulation_output_dir;
			if(~exist(obj.simulation_output_dir, 'dir'))
				mkdir(obj.simulation_output_dir);
			end
		end
		
		% ======================================================================
		%> @brief Set the kes and the des arrays.
		%>
		%> @param kes Array of environment stiffnesses.
		% ======================================================================
		function setKes(obj, k_e_vect)
			obj.tracking.environment.k_e_vect = k_e_vect;			
			obj.tracking.environment.KES_NUM_ELEM = length(k_e_vect);
		end
		
		% ======================================================================
		%> @brief Set the jes and the des arrays.
		%>
		%> @param jes Array of environment inertias.
		% ======================================================================
		function setJes(obj, j_e_vect)
			obj.tracking.environment.j_e_vect = j_e_vect;			
			obj.tracking.environment.JES_NUM_ELEM = length(j_e_vect);
		end
		
		% ======================================================================
		%> @brief Get the actual environment stiffness.
		%>
		%> @return the actual ke.
		% ======================================================================
		function ke = ke(obj)
			ke = obj.tracking.environment.k_e_vect(obj.tracking.environment.index_ke);
		end
		
		% ======================================================================
		%> @brief Get the actual environment inertia.
		%>
		%> @return the actual je.
		% ======================================================================
		function je = je(obj)
			je = obj.tracking.environment.j_e_vect(obj.tracking.environment.index_je);
		end
		
		% ======================================================================
		%> @brief Get the actual environment damping.
		%>
		%> @return the actual de.
		% ======================================================================
		function de = de(obj)
			de = obj.tracking.environment.damping_factor * 2.0 * sqrt(obj.je * obj.ke);
		end
		
		% ======================================================================
		%> @brief Save the instance to a yaml file.
		%>
		%> @param filename The filename where to save the instance.
		%> @param config_name The configuration name.
		% ======================================================================
		function writeToYamlFile(obj, filename, config_name)
			% prepare the structure to write in the yaml file
			P = struct();
			
			allprops = properties(obj);
			for i=1:numel(allprops)
				field = allprops{i};
				P.(config_name).(field) = obj.(field);
			end
			
			% write the struct to the file
			WriteYaml([obj.simulation_output_dir filename '.yaml'],P);
		end
		
		% ======================================================================
		%> @brief Load the instance from a yaml file.
		%>
		%> @param filename The filename to load.
		% ======================================================================
		function readFromYamlFile(obj, filename, config_name)
			% read the struct from the file
			P = ReadYaml([obj.simulation_output_dir	filename '.yaml']);
			
			% convert cells in P to structs
			P = convertCells2structs(P);
			
			obj.config_name = config_name;
			
			% check the configuration name
			if(~isfield(P,config_name))
				error([obj.simulation_output_dir filename '.yaml: configuration name "' config_name '" not found!']);
			end
			
			% take all the fields from the struct
			ff = fieldnames(P.(config_name));
			
			for i=1:length(ff)
				field = ff{i};
				if(isempty(obj.findprop(field)))
					addprop(obj,field);
				end
				
				obj.(field) = P.(config_name).(field);
			end
			
			if(isfield(P.(config_name).transparency,'enable_transparency'))
				obj.transparency.enable_transparency = P.(config_name).transparency.enable_transparency;
			else
				obj.transparency.enable_transparency = false;
			end
			
			obj.setKes(obj.tracking.environment.k_e_vect);
			obj.setJes(obj.tracking.environment.j_e_vect);
			
			if(~isfield(P.(config_name),'h_physics'))
				obj.h_physics = 1 / obj.loop.frequency;
			end
			if(~isfield(P.(config_name),'h_control'))
				obj.h_control = 1 / obj.loop.frequency;
			end
		end
	end
end