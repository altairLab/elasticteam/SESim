% ======================================================================
%> @file configuration.m
%> @brief Configuration file. It creates and sets the parameters, the
% simulation, the model and the controller.
% ======================================================================

%> create the parameters object and read from the .yaml file
params = Parameters(simulation_dir);
params.readFromYamlFile(simulation_name,configuration_name);

% create the simulation object
global simulation
simulation = Simulation('sim_model');
simulation.setParams(params);

% set transparency model
model_transparency = "CurrentTransparency";

% set motor controller
controller = params.motor_controller.name;

% set transparency controller
controller_transparency = 'PID_Controller_transparency';

% set model
model = params.motor.motor_type;

% load utilities signals
utils_signals;

% update the simulation object
simulation.setParams(params);
simulation.setModel(model);
simulation.setController(controller);

% make the simulation output directory if it doesn't exist
if(~exist(params.simulation_output_dir, 'dir'))
	mkdir(params.simulation_output_dir);
end

% change the transparency sweep duration to match the other signal length
params.transparency.reference_signal.sweep_duration = params.tracking.reference_signal.step_duration * 4 + params.tracking.reference_signal.sweep_duration;

% save to .yaml file
params.writeToYamlFile(simulation_name,configuration_name);



